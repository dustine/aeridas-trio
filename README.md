# Aeridas' Trio

Prima's candlenights gift for Nabiva, and everyone else as well. 😉

An (extremely) Aeridas themed [Iso-path](https://www.youtube.com/watch?v=Wz6q03b8R6U) implementation on Phaser 3, with a Twine story introduction.

_Release date_: December 25th, 2019. Here's hoping.

## Components

### [Prelude](https://dustine.gitlab.io/aeridas-trio/)

Coded entirely on [Twine](http://twinery.org/), with a few scripts so to make it seem more like Dungeons and Dragons.

### [Board Game](https://dustine.gitlab.io/aeridas-trio/game)

- [Phaser 3](https://phaser.io/)
  - "Forked" from [Phaser 3 Typescript Project Template](https://github.com/photonstorm/phaser3-typescript-project-template/)
  - [Phaser Super Storage](https://github.com/azerion/phaser-super-storage) Plugin
- [Honeycomb Grid](https://github.com/flauwekeul/honeycomb)
- [Webfont Loader](https://github.com/typekit/webfontloader)

## Available Commands

| Command         | Description                                                                       |
| --------------- | --------------------------------------------------------------------------------- |
| `npm install`   | Install project dependencies (use `--production` to install build-only packages   |
| `npm run watch` | Build project and open web server running project, watching for changes           |
| `npm run dev`   | Builds project and open web server, but do not watch for changes                  |
| `npm run build` | Builds code bundle with production settings (minification, no source maps, etc..) |

## Thank yous

- To my DnD group in general for being such cool and creative peeps, the world and the characters are all theirs after all!
  - Zach for being an awesome co-conspirator and feedback machine;
  - Jean for the character emoji, they were perfect for the faces;
  - And last, but not least, Hex for DMing us in this wonderful world for the past three years.
- To Sollay for completing the character emojis, without you I'd still be doing them by hand!
- The Fungeon, for help with the code and motivation.
