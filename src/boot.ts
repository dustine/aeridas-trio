import LoadingScene from "./loading";
import BoardScene from "./board/boardScene";
import MenuScene from "./menu";

export default class BootScene extends Phaser.Scene {
  preload(): void {
    this.load.json("players", "assets/players.json");
  }

  create(): void {
    const players = this.cache.json.get("players");

    // sanity check for unique id's
    players.forEach((p1: { id: string }, i: number) => {
      const otherPlayers = players
        .slice(0, i)
        .concat(players.slice(i + 1, players.length));

      if (
        otherPlayers.find(p2 => {
          return p1.id == p2.id;
        })
      ) {
        throw `Conflicting player IDs ${p1.id}`;
      }
    });

    this.scene.add("LoadingScene", LoadingScene, true);
    this.scene.add("MenuScene", MenuScene, false);
    this.scene.add("BoardScene", BoardScene, false);
    this.scene.stop();
  }
}
