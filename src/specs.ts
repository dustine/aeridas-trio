export type PlayerScaffold = {
  id: string;
  name: string;
  base: number;
  ribbon: number;
};

export type Dialog = {
  id?: string;
  characters: string[];
  convos: Array<string[]>;
};

export type BoardSceneData = {
  layout: BoardLayout;
  bottomPlayer: PlayerScaffold;
  topPlayer: PlayerScaffold;
  middlePlayer?: PlayerScaffold;
  cpu: boolean[];
  dialogs: Dialog[];
  highlight: boolean;
};

export enum BoardLayout {
  Duo8 = 0,
  Duo10 = 1,
  Duo12 = 2,
  Trio12 = 3,
  Duo = 0,
  Trio = 3
}

export enum PlayerHeight {
  Bottom = 0,
  Top = 1,
  Middle = 2
}

// the export for these two is broken and it seems it's not as easy as adding a namedExport
// so better to just redeclare them >o<
export enum PointyCompassDirection {
  E = 0,
  SE = 1,
  SW = 2,
  W = 3,
  NW = 4,
  NE = 5
}

export enum FlatCompassDirection {
  SE = 0,
  S = 1,
  SW = 2,
  NW = 3,
  N = 4,
  NE = 5
}

export const playerReg = /^\d+@/i;
