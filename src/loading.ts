import PhaserSuperStorage from "@azerion/phaser-super-storage";

export default class LoadingScene extends Phaser.Scene {
  storage: PhaserSuperStorage;

  preload(): void {
    const progress = this.add.graphics();

    this.load.on("progress", function(value: number) {
      progress.clear();
      progress.fillStyle(0xffffff, 1);
      progress.fillRect(0, 270, 800 * value, 60);
    });

    this.load.on(
      "complete",
      function() {
        const storage = this.plugins.get("SuperStorage") as PhaserSuperStorage;
        storage.setNamespace("aeridas-trio");

        progress.clear();
        this.scene.start("MenuScene");
      },
      this
    );

    // player select stuff
    this.cache.json.get("players").forEach(element => {
      this.load.image(
        `player:${element.id}`,
        `assets/players/${element.id}.png`
      );
    });

    this.load.setPrefix("menu:");
    let root = "assets/menu/";

    this.load.image("play", root + "play.png");
    this.load.image("duo8", root + "duo8.png");
    this.load.image("duo10", root + "duo10.png");
    this.load.image("duo12", root + "duo12.png");
    this.load.image("trio12", root + "trio12.png");
    this.load.image("layout", root + "layout.png");
    this.load.image("layoutSelected", root + "layoutSelected.png");

    this.load.image("playerBase", root + "playerBase.png");
    this.load.image("playerBorder", root + "playerBorder.png");
    this.load.image("playerSelected", root + "playerSelected.png");
    this.load.image("randomPlayer", root + "randomPlayer.png");
    this.load.image("gonePlayer", root + "gonePlayer.png");
    this.load.image("unnecessaryPlayer", root + "unnecessaryPlayer.png");

    this.load.setPrefix("menu:color:");
    root = "assets/menu/customColor/";

    this.load.image("background", root + "background.png");
    this.load.image("base", root + "base.png");
    this.load.image("ribbon", root + "ribbon.png");
    this.load.image("reset", root + "reset.png");
    this.load.image("preview", root + "preview.png");
    this.load.image("slider", root + "slider.png");
    this.load.image("pip", root + "pip.png");

    this.load.setPrefix("menu:rules:");
    root = "assets/menu/rules/";

    this.load.image("background", root + "background.png");
    this.load.image("bottomBase", root + "bottomBase.png");
    this.load.image("bottomRibbon", root + "bottomRibbon.png");
    this.load.image("topBase", root + "topBase.png");
    this.load.image("topRibbon", root + "topRibbon.png");
    this.load.image("middleBase", root + "middleBase.png");
    this.load.image("middleRibbon", root + "middleRibbon.png");

    this.load.setPrefix();
    this.load.json("dialog", "assets/dialog.json");

    // board ui stuff
    this.load.setPrefix("board:");
    root = "assets/board/";

    this.load.image("playerMarker", root + "player_marker.png");
    this.load.image("moveNone", root + "move_none.png");
    this.load.image("moveStone", root + "move_stone.png");
    this.load.image("moveTile", root + "move_tile.png");
    this.load.image("moveCapture", root + "move_capture.png");

    this.load.image("dialog", root + "dialog.png");
    this.load.image("dialogProng", root + "dialog_prong.png");

    this.load.image("exit", root + "exit.png");
    this.load.image("mute", root + "mute.png");
    this.load.image("sound", root + "sound.png");
    this.load.image("plain", root + "plain.png");
    this.load.image("highlight", root + "highlight.png");

    // board stuff
    this.load.image("stoneBase", root + "stoneBase.png");
    this.load.image("stoneRibbon", root + "stoneRibbon.png");

    root = "assets/board/border/";
    this.load.image("border_nOn", root + "n_on.png");
    this.load.image("border_nRibbon", root + "n_ribbon.png");
    this.load.image("border_nOff", root + "n_off.png");

    this.load.image("border_neOn", root + "ne_on.png");
    this.load.image("border_neRibbon", root + "ne_ribbon.png");
    this.load.image("border_neOff", root + "ne_off.png");

    this.load.image("border_seOn", root + "se_on.png");
    this.load.image("border_seRibbon", root + "se_ribbon.png");
    this.load.image("border_seOff", root + "se_off.png");

    this.load.image("border_sOn", root + "s_on.png");
    this.load.image("border_sRibbon", root + "s_ribbon.png");
    this.load.image("border_sOff", root + "s_off.png");

    root = "assets/board/tiles/";
    this.load.image("tileBottom", root + "bottom.png");
    this.load.image("tileBottomSelected", root + "bottomSelected.png");
    this.load.image("tileBottomSource", root + "bottomSource.png");
    this.load.image("tileBottomTarget", root + "bottomTarget.png");
    this.load.image("tileBottomCapture", root + "bottomCapture.png");

    this.load.image("tileRise", root + "rise.png");

    this.load.image("tileMiddle", root + "middle.png");
    this.load.image("tileMiddleSelected", root + "middleSelected.png");
    this.load.image("tileMiddleSource", root + "middleSource.png");
    this.load.image("tileMiddleTarget", root + "middleTarget.png");
    this.load.image("tileMiddleCapture", root + "middleCapture.png");

    this.load.image("tileTop", root + "top.png");
    this.load.image("tileTopSelected", root + "topSelected.png");
    this.load.image("tileTopSource", root + "topSource.png");
    this.load.image("tileTopTarget", root + "topTarget.png");
    this.load.image("tileTopCapture", root + "topCapture.png");

    this.load.image("tileFull", root + "full.png");
  }
}
