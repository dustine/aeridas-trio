import PhaserSuperStorage from "@azerion/phaser-super-storage";
import { GameObjects, Input } from "phaser";
import shuffle from "./shuffle";
import { BoardLayout, Dialog, PlayerScaffold, BoardSceneData } from "./specs";

const width = 864;
const height = 672;
const playerDepth = 10;
const dragDepth = 20;

enum PlayerSlot {
  None = 0,
  Bottom = 1,
  Top = 2,
  Middle = 3
}

enum ColorMask {
  Red = 255 << 16,
  Green = 255 << 8,
  Blue = 255 << 0
}

enum CustomColor {
  None = 0,
  Base = 2,
  Ribbon = 3,
  OnlyBase = 6,
  OnlyRibbon = 7
}

function HSVtoRGB(h, s, v): { r: number; g: number; b: number } {
  let r, g, b;
  if (arguments.length === 1) {
    (s = h.s), (v = h.v), (h = h.h);
  }
  const i = Math.floor(h * 6);
  const f = h * 6 - i;
  const p = v * (1 - s);
  const q = v * (1 - f * s);
  const t = v * (1 - (1 - f) * s);
  switch (i % 6) {
    case 0:
      (r = v), (g = t), (b = p);
      break;
    case 1:
      (r = q), (g = v), (b = p);
      break;
    case 2:
      (r = p), (g = v), (b = t);
      break;
    case 3:
      (r = p), (g = q), (b = v);
      break;
    case 4:
      (r = t), (g = p), (b = v);
      break;
    case 5:
      (r = v), (g = p), (b = q);
      break;
  }
  return {
    r: Math.round(r * 255),
    g: Math.round(g * 255),
    b: Math.round(b * 255)
  };
}

export default class MenuScene extends Phaser.Scene {
  storage: PhaserSuperStorage;
  boardLayout: BoardLayout;
  playerSpecs: { [x: string]: PlayerScaffold };
  colorOverrides: {
    [x: string]: {
      base?: (player?: PlayerScaffold) => number;
      ribbon?: (player?: PlayerScaffold) => number;
    };
  };

  private duo: GameObjects.Image;
  private duo10: GameObjects.Image;
  private duo12: GameObjects.Image;
  private trio: GameObjects.Image;

  chosenPlayers: { [x: number]: GameObjects.Container };
  slots: { [x: number]: GameObjects.Image };
  playerNames: GameObjects.Text;
  players: GameObjects.Group;
  customColors: GameObjects.Container;
  rules: GameObjects.Container;

  preload(): void {
    this.storage = this.plugins.get("SuperStorage") as PhaserSuperStorage;
  }

  init(): void {
    this.input.dragDistanceThreshold = 4;
  }

  create(): void {
    this.chosenPlayers = {
      [PlayerSlot.Bottom]: undefined as GameObjects.Container,
      [PlayerSlot.Top]: undefined as GameObjects.Container,
      [PlayerSlot.Middle]: undefined as GameObjects.Container
    };
    this.slots = {
      [PlayerSlot.Bottom]: undefined as GameObjects.Image,
      [PlayerSlot.Top]: undefined as GameObjects.Image,
      [PlayerSlot.Middle]: undefined as GameObjects.Image
    };
    this.colorOverrides = {
      // Prim will always use the default colours; xe's not your character, after all.
      pri: {
        base: function(): number {
          return this.playerSpecs.pri.base;
        }.bind(this),
        ribbon: function(): number {
          return this.playerSpecs.pri.ribbon;
        }.bind(this)
      },
      // Aelemar will *refuse* to play along, and hence the stones will stay on the default colours
      ael: {
        base: function(): number {
          return undefined;
        }.bind(this),
        ribbon: function(): number {
          return undefined;
        }.bind(this)
      },
      // Iris's ribbon will be a random max-saturated color
      iri: {
        ribbon: function(): number {
          const rgb = HSVtoRGB(Math.random(), 1, 1);
          return parseInt(
            ((1 << 24) + (rgb.r << 16) + (rgb.g << 8) + rgb.b)
              .toString(16)
              .slice(1),
            16
          );
        }.bind(this)
      }
    };

    // ## Board Mode ##
    let x = width - 128;
    let y = 8;
    this.createLayoutButtons(x, y);

    // ## Selected Players ##
    x = this.duo.getTopLeft().x / 2;
    y += 8;
    this.createPlayerSlots(x, y);

    // ## Player Box ##
    x = 16;
    y = this.duo.getBottomLeft().y + 24;
    // y = height - 5 * 8 - 6 * 75 - 8;
    this.createPlayerBox(x, y);

    // ## Custom Colors ##
    x = width - 8;
    y = this.duo.getBottomLeft().y + 16;
    this.createCustomColor(x, y);

    // ## Rules
    x = width - 8;
    y += (this.customColors.first as GameObjects.Image).height + 16;
    this.createRules(x, y);

    // ## Start game
    const start = this.add
      .image(width - 16, height - 8, "menu:play")
      .setInteractive()
      .setOrigin(1, 1);
    start.input.cursor = "pointer";
    start.on(
      "pointerdown",
      () => {
        this.scene.start("BoardScene", this.generateBoardData());
        // this.scene.transition({
        //   target: "BoardScene",
        //   sleep: true,
        //   data: that.getStatusData()
        // });
      },
      this
    );

    //// ## Custom Player Settings
    const iris = this.findPlayer("iri");
    if (iris) {
      (iris.list[1] as GameObjects.Sprite).setTint(
        0xff0000,
        0xffff00,
        0x0000ff,
        0x00ff00
      );
    }

    // ## Final Touches + state ##
    // preset game starts
    this.boardLayout = undefined;
    // this.selectedPlayer = undefined;

    const secret = parseInt(this.storage.getItem("rinolin"));
    if (!secret) {
      this.findPlayer("rin").destroy();
    }

    const storyStart = this.storage.getItem("storyStart");
    if (storyStart) {
      this.setBoardType(BoardLayout.Trio);
      this.movePlayer("nab", PlayerSlot.Bottom);
      this.movePlayer("nae", PlayerSlot.Top);
      this.movePlayer("pri", PlayerSlot.Middle);
    } else {
      this.setBoardType(BoardLayout.Duo);
    }

    this.updatePlayerNames();
    this.disableCustomColors();
    this.transitionTo(new NeutralState());
  }

  private createGamemodeButton(
    x: number,
    y: number,
    image: string,
    type: BoardLayout
  ): GameObjects.Image {
    const button = this.add
      .image(x, y, "menu:layout")
      .setOrigin(0, 0)
      .setData("type", type)
      .setInteractive();
    this.add.image(button.getCenter().x, button.getCenter().y, image);
    button.input.cursor = "pointer";
    button.on("pointerup", this.onGamemodePointerUp.bind(this, type));

    return button;
  }

  private createPlayerButton(
    x: number,
    y: number,
    player: PlayerScaffold
  ): GameObjects.Container {
    const originalBase = player.base;
    const originalRibbon = player.ribbon;

    const storedBase = parseInt(this.storage.getItem(`pb:${player.id}`));
    if (storedBase) {
      player.base = storedBase;
    }

    const base = this.add.image(0, 0, "menu:playerBase").setTint(player.base);

    const storedRibbon = parseInt(this.storage.getItem(`pr:${player.id}`));
    if (storedRibbon) {
      player.ribbon = storedRibbon;
    }

    const frame = this.add
      .image(0, 0, "menu:playerBorder")
      .setTint(player.ribbon);
    const head = this.add.image(0, 0, `player:${player.id}`);

    const button = this.add
      .container(x, y, [base, frame, head])
      .setDepth(playerDepth)
      .setState(PlayerSlot.None)
      .setName(player.id)
      .setData("player", player)
      .setData("home", { x: x, y: y })
      .setData("originalBase", originalBase)
      .setData("originalRibbon", originalRibbon)
      .setInteractive(
        new Phaser.Geom.Rectangle(
          -frame.width / 2,
          -frame.height / 2,
          frame.width,
          frame.height
        ),
        Phaser.Geom.Rectangle.Contains
      );

    button.input.cursor = "pointer";
    this.input.setDraggable(button);

    button.on("pointerup", this.onPlayerPointerUp.bind(this, button));
    button.on("pointerdown", this.onPlayerPointerDown.bind(this, button));

    button.on("dragstart", this.onPlayerDragStart.bind(this, button));
    button.on("dragenter", this.onPlayerDragEnter.bind(this, button));
    button.on("drag", this.onPlayerDrag.bind(this, button));
    button.on("dragleave", this.onPlayerDragLeave.bind(this, button));
    button.on("dragend", this.onPlayerDragEnd.bind(this, button));

    this.players.add(button);
    return button;
  }

  private createColorBar(
    x: number,
    y: number,
    mask: ColorMask
  ): GameObjects.Image {
    const slider = this.add.image(x, y, "menu:color:slider");
    // let pip = that.add.image(-slider.width / 2, 0, "menu:color:pip");

    // let hx = slider.width + pip.width;
    // let hy = pip.height;

    slider.setInteractive({
      hitArea: new Phaser.Geom.Rectangle(
        -8,
        -4,
        slider.width + 16,
        slider.height + 8
      ),
      hitAreaCallback: Phaser.Geom.Rectangle.Contains,
      useHandCursor: true
    });
    slider.on("pointerdown", this.onSliderPointerDown.bind(this, slider, mask));

    return slider;
    // return that.add.container(x, y, [slider, pip]);
  }

  private createColorTargetButton(
    x: number,
    y: number,
    type: string,
    pointerdown: Function
  ): GameObjects.Container {
    const background = this.add.sprite(0, 0, `menu:color:${type}`);
    const preview = this.add.sprite(42, 0, "menu:color:preview");

    const button = this.add.container(x, y, [background, preview]);

    const hx = background.width;
    const hy = background.height;

    button
      .setInteractive({
        hitArea: new Phaser.Geom.Rectangle(-hx / 2, -hy / 2, hx, hy),
        hitAreaCallback: Phaser.Geom.Rectangle.Contains,
        useHandCursor: true
      })
      .on("pointerdown", pointerdown, this);

    return button;
  }

  private createLayoutButtons(x: number, y: number): void {
    const trio = (this.trio = this.createGamemodeButton(
      x,
      y,
      "menu:trio12",
      BoardLayout.Trio
    ));
    x -= 16;

    this.add
      .line(x, y + 8, 0, 0, 0, trio.height - 16, 0x4d4d4d)
      .setLineWidth(0.5)
      .setOrigin(0, 0);
    x -= trio.width + 16;

    this.duo12 = this.createGamemodeButton(
      x,
      y,
      "menu:duo12",
      BoardLayout.Duo12
    );
    x -= trio.width + 16;

    this.duo10 = this.createGamemodeButton(
      x,
      y,
      "menu:duo10",
      BoardLayout.Duo10
    );
    x -= trio.width + 16;

    this.duo = this.createGamemodeButton(x, y, "menu:duo8", BoardLayout.Duo);
    x -= trio.width + 16;
  }

  private createPlayerSlots(x: number, y: number): void {
    const topSlot = this.add
      .image(x, y, "menu:randomPlayer")
      .setOrigin(0.5, 0)
      .setName("slotTop")
      .setState(PlayerSlot.Top)
      .setInteractive({
        dropZone: true
      });
    this.slots[PlayerSlot.Top] = topSlot;
    const bottomSlot = this.add
      .image(x - topSlot.width - 8, topSlot.getCenter().y, "menu:randomPlayer")
      .setName("slotBottom")
      .setState(PlayerSlot.Bottom)
      .setInteractive({
        dropZone: true
      });
    this.slots[PlayerSlot.Bottom] = bottomSlot;
    const middleSlot = this.add
      .image(
        x + topSlot.width + 8,
        topSlot.getCenter().y,
        "menu:unnecessaryPlayer"
      )
      .setName("slotMiddle")
      .setState(PlayerSlot.Middle)
      .setInteractive({
        dropZone: true
      })
      .setName("playerSlot");
    this.slots[PlayerSlot.Middle] = middleSlot;

    y += topSlot.height + 8;

    const playerNames = this.add
      .text(x, y, "?")
      .setColor("black")
      .setFontFamily("Solway")
      .setFontStyle("300")
      .setFontSize(20)
      .setData("x", x);
    this.playerNames = playerNames;
  }

  private createPlayerBox(x: number, y: number): void {
    const topSlot = this.slots[PlayerSlot.Top];
    this.players = this.add.group();

    const playerSpecs = this.cache.json
      .get("players")
      .map((e: { base: string; ribbon: string; id: string; name: string }) => {
        let base = undefined;
        if (e.base.length != 0) {
          base = parseInt(e.base.replace("#", "0x"));
        }

        let ribbon = undefined;
        if (e.ribbon.length != 0) {
          ribbon = parseInt(e.ribbon.replace("#", "0x"));
        }

        return {
          id: e.id,
          name: e.name,
          base: base,
          ribbon: ribbon
        } as PlayerScaffold;
      });

    this.playerSpecs = {};

    playerSpecs.forEach((ps: PlayerScaffold, i: number) => {
      const localx = x + (i % 5) * (topSlot.width + 8) + topSlot.width / 2;
      const localy =
        y + Math.floor(i / 5) * (topSlot.height + 8) + topSlot.height / 2;

      this.add
        .image(localx, localy, "menu:gonePlayer")
        .setName(ps.id)
        .setInteractive(undefined, undefined, true);
      // use a local copy of the spec for custom color support
      this.createPlayerButton(localx, localy, ps);
      this.playerSpecs[ps.id] = ps;
    });
  }

  private createCustomColor(x: number, y: number): void {
    const colorBackground = this.add.image(0, 0, "menu:color:background");
    const color = (this.customColors = this.add.container(
      x,
      y,
      colorBackground
    ));
    color.x -= colorBackground.width / 2;
    color.y += colorBackground.height / 2;

    let ry = -colorBackground.height / 2 + 52;

    const baseButton = this.createColorTargetButton(
      -colorBackground.width / 4 - 28,
      ry,
      "base",
      this.onColorBasePointerDown
    );
    const ribbonButton = this.createColorTargetButton(
      0,
      ry,
      "ribbon",
      this.onColorRibbonPointerDown
    );
    const resetButton = this.add
      .sprite(colorBackground.width / 4 + 28, ry, "menu:color:reset")
      .setInteractive();
    resetButton.input.cursor = "pointer";
    resetButton.on("pointerdown", this.onColorResetPointerDown, this);

    ry = colorBackground.height / 2 - 20 - 32 * 2;
    const redBar = this.createColorBar(0, ry, ColorMask.Red);
    const redPip = this.add.image(0, ry, "menu:color:pip");
    ry += 32;
    const greenBar = this.createColorBar(0, ry, ColorMask.Green);
    const greenPip = this.add.image(0, ry, "menu:color:pip");
    ry += 32;
    const blueBar = this.createColorBar(0, ry, ColorMask.Blue);
    const bluePip = this.add.image(0, ry, "menu:color:pip");

    color.add([
      baseButton,
      ribbonButton,
      resetButton,
      redBar,
      greenBar,
      blueBar,
      redPip,
      greenPip,
      bluePip
    ]);
  }

  private createRules(x: number, y: number): void {
    const rules = (this.rules = this.add.container(x, y, [
      this.add.image(0, 0, "menu:rules:background"),
      this.add.image(0, 0, "menu:rules:bottomBase"),
      this.add.image(0, 0, "menu:rules:bottomRibbon"),
      this.add.image(0, 0, "menu:rules:topBase"),
      this.add.image(0, 0, "menu:rules:topRibbon"),
      this.add.image(0, 0, "menu:rules:middleBase"),
      this.add.image(0, 0, "menu:rules:middleRibbon")
    ]));
    rules.x -= (rules.first as GameObjects.Image).width / 2;
    rules.y += (rules.first as GameObjects.Image).height / 2;
  }

  setBoardType(boardType: BoardLayout): void {
    this.duo.setTexture("menu:layout").setInteractive();
    this.duo10.setTexture("menu:layout").setInteractive();
    this.duo12.setTexture("menu:layout").setInteractive();
    this.trio.setTexture("menu:layout").setInteractive();

    if (boardType != BoardLayout.Trio) {
      this.clearSlot(PlayerSlot.Middle);
      this.slots[PlayerSlot.Middle].setTexture("menu:unnecessaryPlayer");
    } else {
      this.slots[PlayerSlot.Middle].setTexture("menu:randomPlayer");
    }

    switch (boardType) {
      case BoardLayout.Duo8:
        this.duo.setTexture("menu:layoutSelected").disableInteractive();
        break;
      case BoardLayout.Duo10:
        this.duo10.setTexture("menu:layoutSelected").disableInteractive();
        break;
      case BoardLayout.Duo12:
        this.duo12.setTexture("menu:layoutSelected").disableInteractive();
        break;
      case BoardLayout.Trio12:
        this.trio.setTexture("menu:layoutSelected").disableInteractive();
        break;
    }

    this.boardLayout = boardType;
    this.updatePlayerNames();
  }

  getFirstEmptySlot(): PlayerSlot {
    if (!this.chosenPlayers[PlayerSlot.Bottom]) return PlayerSlot.Bottom;
    if (!this.chosenPlayers[PlayerSlot.Top]) return PlayerSlot.Top;
    if (
      this.boardLayout == BoardLayout.Trio &&
      !this.chosenPlayers[PlayerSlot.Middle]
    )
      return PlayerSlot.Middle;
    return PlayerSlot.None;
  }

  clearSlot(slot: PlayerSlot): void {
    const player = this.chosenPlayers[slot];
    if (player) {
      this.chosenPlayers[slot] = undefined;
      this.resetPlayer(player);
    }
  }

  private resetPlayer(player: GameObjects.Container): void {
    const home = player.data.values.home;

    player
      .setState(PlayerSlot.None)
      .setX(home.x)
      .setY(home.y);

    this.updatePlayerNames();
    this.updateRules();
  }

  getPlayerName(slot: PlayerSlot): string {
    const player = this.chosenPlayers[slot] as GameObjects.Container;
    if (player) {
      return player.data.values.player.name;
    } else {
      return "🎲";
    }
  }

  updatePlayerNames(): string {
    let string = `${this.getPlayerName(
      PlayerSlot.Bottom
    )}, ${this.getPlayerName(PlayerSlot.Top)}`;

    if (this.boardLayout == BoardLayout.Trio) {
      string += `, ${this.getPlayerName(PlayerSlot.Middle)}`;
    }

    this.playerNames
      .setText(string)
      .setX(
        this.playerNames.data.values.x - Math.round(this.playerNames.width / 2)
      );

    return string;
  }

  findPlayer(id: string): GameObjects.Container {
    return this.players.getChildren().find(p => {
      return p.name == id;
    }) as GameObjects.Container;
  }

  movePlayer(player: string | GameObjects.Container, target: PlayerSlot): void {
    if (typeof player == "string") {
      player = this.findPlayer(player);
      if (!player) return;
    } else {
      player = player as GameObjects.Container;
    }

    const source = player.state as PlayerSlot;

    if (target == PlayerSlot.None) {
      if (source == PlayerSlot.None) {
        // player reset (from drags)
        this.resetPlayer(player);
      } else {
        // double click deselection
        this.clearSlot(source);
        this.updateRules();
      }
      return;
    }

    // won't move a player to the third slot if we're not on a Trio board
    if (this.boardLayout != BoardLayout.Trio && target == PlayerSlot.Middle) {
      this.movePlayer(player, PlayerSlot.None);
      return;
    }

    if (source == PlayerSlot.None) {
      // selection
      this.clearSlot(target);
    } else if (this.chosenPlayers[target]) {
      // player swap (first)
      const sourceCoords = this.slots[source].getCenter();

      this.chosenPlayers[source] = this.chosenPlayers[target]
        .setState(source)
        .setX(sourceCoords.x)
        .setY(sourceCoords.y);
    } else {
      // clear the source if we didn't swap a player there
      this.clearSlot(source);
    }
    // slot swap
    const targetCoords = this.slots[target].getCenter();
    this.chosenPlayers[target] = player
      .setState(target)
      .setX(targetCoords.x)
      .setY(targetCoords.y);

    this.updatePlayerNames();
    this.updateRules();
  }

  enableCustomColors(
    player: PlayerScaffold,
    target?: CustomColor
  ): CustomColor {
    let colorTargetType = target;
    const colorException = this.colorOverrides[player.id];

    if (colorException) {
      if (colorException.base) {
        if (colorException.ribbon) {
          // don't do anything
          return CustomColor.None;
        } else {
          colorTargetType = CustomColor.OnlyRibbon;
        }
      } else {
        if (colorException.ribbon) {
          colorTargetType = CustomColor.OnlyBase;
        }
      }
    }

    (this.customColors.list[0] as GameObjects.Image).clearTint();

    const baseButton = this.customColors.list[1] as GameObjects.Container;
    const baseBackground = baseButton.first as GameObjects.Image;
    const basePreview = baseButton.last as GameObjects.Image;
    const ribbonButton = this.customColors.list[2] as GameObjects.Container;
    const ribbonBackground = ribbonButton.first as GameObjects.Image;
    const ribbonPreview = ribbonButton.last as GameObjects.Image;

    const baseColor = player.base ? player.base : 0xffffff;
    const ribbonColor = player.ribbon ? player.ribbon : 0xffffff;

    switch (colorTargetType) {
      default:
        // need default in case it's still undefined by now
        colorTargetType = CustomColor.Base;
        baseButton.disableInteractive();
        baseBackground.setTint(0xffd42a);
        basePreview.setTint(baseColor);

        ribbonButton.setInteractive();
        ribbonBackground.clearTint();
        ribbonPreview.setTint(ribbonColor);
        break;
      case CustomColor.Base:
        baseButton.disableInteractive();
        baseBackground.setTint(0xffd42a);
        basePreview.setTint(baseColor);

        ribbonButton.setInteractive();
        ribbonBackground.clearTint();
        ribbonPreview.setTint(ribbonColor);
        break;
      case CustomColor.Ribbon:
        baseButton.setInteractive();
        baseBackground.clearTint();
        basePreview.setTint(baseColor);

        ribbonButton.disableInteractive();
        ribbonBackground.setTint(0xffd42a);
        ribbonPreview.setTint(ribbonColor);
        break;

      case CustomColor.OnlyBase:
        baseButton.disableInteractive();
        baseBackground.setTint(0xffd42a);
        basePreview.setTint(baseColor);

        ribbonButton.disableInteractive();
        ribbonBackground.setTint(0x333333);
        break;
      case CustomColor.OnlyRibbon:
        baseButton.disableInteractive();
        baseBackground.setTint(0x333333);

        ribbonButton.disableInteractive();
        ribbonBackground.setTint(0xffd42a);
        ribbonPreview.setTint(ribbonColor);
        break;
    }

    (this.customColors.list[3] as GameObjects.Image)
      .clearTint()
      .setInteractive();

    // clears the tint on the pips
    for (let index = 4; index < 10; index++) {
      const image = this.customColors.list[index] as GameObjects.Image;
      if (index < 7) {
        image.setInteractive();
      } else {
        image.clearTint();
      }
    }
    this.updateCustomColors(player, colorTargetType);

    return colorTargetType;
  }

  updateCustomColors(player: PlayerScaffold, target: CustomColor): void {
    const baseButton = this.customColors.list[1] as GameObjects.Container;
    const basePreview = baseButton.last as GameObjects.Image;
    const ribbonButton = this.customColors.list[2] as GameObjects.Container;
    const ribbonPreview = ribbonButton.last as GameObjects.Image;

    const baseColor = player.base ? player.base : 0xffffff;
    const ribbonColor = player.ribbon ? player.ribbon : 0xffffff;

    if (target != CustomColor.OnlyRibbon) {
      basePreview.setTint(baseColor);
    }
    if (target != CustomColor.OnlyBase) {
      ribbonPreview.setTint(ribbonColor);
    }

    let color: number;
    switch (target) {
      case CustomColor.Base:
      case CustomColor.OnlyBase:
        color = baseColor;
        break;
      case CustomColor.Ribbon:
      case CustomColor.OnlyRibbon:
        color = ribbonColor;
        break;
    }

    const colorTypes = [ColorMask.Red, ColorMask.Green, ColorMask.Blue];

    for (let index = 0; index < colorTypes.length; index++) {
      const type = colorTypes[index];

      const slider = this.customColors.list[4 + index] as GameObjects.Image;
      slider.setTint(color & ~type, color | type, color & ~type, color | type);

      const pip = this.customColors.list[7 + index] as GameObjects.Image;

      const x =
        (((color & type) >> ((colorTypes.length - index - 1) * 8)) / 255) *
          slider.width -
        slider.width / 2;
      pip.clearTint().setX(x);
    }
  }

  disableCustomColors(): void {
    (this.customColors.list[0] as GameObjects.Image).setTint(0x333333);
    const baseButton = (this.customColors
      .list[1] as GameObjects.Container).disableInteractive();
    (baseButton.first as GameObjects.Image).setTint(0x444444);
    (baseButton.last as GameObjects.Image).setTint(0x444444);
    const ribbonButton = (this.customColors
      .list[2] as GameObjects.Container).disableInteractive();
    (ribbonButton.first as GameObjects.Image).setTint(0x444444);
    (ribbonButton.last as GameObjects.Image).setTint(0x444444);
    for (let index = 3; index < 10; index++) {
      (this.customColors.list[index] as GameObjects.Image)
        .setTint(0x444444)
        .disableInteractive();
    }
  }

  updateRules(slot?: PlayerSlot, target?: CustomColor): void {
    const players = [
      this.chosenPlayers[PlayerSlot.Bottom],
      this.chosenPlayers[PlayerSlot.Top],
      this.chosenPlayers[PlayerSlot.Middle]
    ];

    players.forEach((p, i) => {
      const rulesBase = this.rules.list[2 * i + 1] as GameObjects.Image;
      const rulesRibbon = this.rules.list[2 * i + 2] as GameObjects.Image;

      if (p) {
        // if there's a slot defined, do only for that slot
        if (!slot || p.state == slot) {
          const player = p.data.values.player as PlayerScaffold;
          const override = this.colorOverrides[player.id];
          let base = player.base;
          let ribbon = player.ribbon;
          if (override) {
            if (override.base) {
              base = override.base(player);
            }
            if (override.ribbon) {
              ribbon = override.ribbon(player);
            }
          }

          // if there's a target defined, do only for that target
          switch (target) {
            case CustomColor.Base:
            case CustomColor.OnlyBase:
              rulesBase.setTint(base);
              break;
            case CustomColor.Ribbon:
            case CustomColor.OnlyRibbon:
              rulesRibbon.setTint(ribbon);
              break;
            default:
              rulesBase.setTint(base);
              rulesRibbon.setTint(ribbon);
          }
        }
      } else {
        switch (target) {
          case CustomColor.Base:
          case CustomColor.OnlyBase:
            rulesBase.clearTint();
            break;
          case CustomColor.Ribbon:
          case CustomColor.OnlyRibbon:
            rulesRibbon.clearTint();
            break;
          default:
            rulesBase.clearTint();
            rulesRibbon.clearTint();
        }
      }
    }, this);
  }

  private finalizePlayer(
    unnassignedPlayers: PlayerScaffold[],
    slot: PlayerSlot
  ): PlayerScaffold {
    const playerButton = this.chosenPlayers[slot];
    let player: PlayerScaffold;
    if (!playerButton) {
      player = unnassignedPlayers.splice(
        Math.floor(Math.random() * unnassignedPlayers.length)
      )[0];
    } else {
      player = playerButton.data.values.player;
    }

    // special cases
    const colorOverride = this.colorOverrides[player.id];
    if (colorOverride) {
      if (colorOverride.base) {
        player.base = colorOverride.base(player);
      } else if (colorOverride.ribbon) {
        player.ribbon = colorOverride.ribbon(player);
      }
    }

    return player;
  }

  private sortDialogId(ids: string[]): string {
    const players = this.players.getChildren().map(ps => {
      return ps.data.values.player.id;
    });
    return ids
      .sort((ls, rs) => {
        return players.indexOf(ls) - players.indexOf(rs);
      })
      .join(",");
  }

  private findDialog(
    p1: PlayerScaffold,
    p2: PlayerScaffold,
    p3?: PlayerScaffold
  ): Dialog {
    const targets = [p1, p2, p3]
      .filter(ps => ps)
      .map(ps => {
        return ps.id;
      });
    const id = this.sortDialogId(targets);
    const dialog = this.cache.json.get("dialog")[id] as Dialog;
    if (dialog) {
      dialog.id = id;
    }
    return dialog;
  }

  generateBoardData(): BoardSceneData {
    const secret = parseInt(this.storage.getItem("rinolin"));
    const otherPlayers = this.players
      .getChildren()
      .filter(p => {
        return p.state == PlayerSlot.None;
      })
      .map(p => {
        return p.data.values.player as PlayerScaffold;
      });

    let bottomPlayer = this.finalizePlayer(otherPlayers, PlayerSlot.Bottom);
    let topPlayer = this.finalizePlayer(otherPlayers, PlayerSlot.Top);
    let middlePlayer = undefined as PlayerScaffold;
    if (this.boardLayout == BoardLayout.Trio) {
      middlePlayer = this.finalizePlayer(otherPlayers, PlayerSlot.Middle);
    } else if (!secret) {
      // check for unlock, force unlock if so
      let counter = parseInt(this.storage.getItem("counter"));
      if (isNaN(counter)) {
        counter = 0;
      }

      let flag = PlayerSlot.None;
      if (bottomPlayer.id == "pri" && !this.chosenPlayers[PlayerSlot.Top]) {
        ++counter;
        flag = PlayerSlot.Top;
      } else if (
        topPlayer.id == "pri" &&
        !this.chosenPlayers[PlayerSlot.Bottom]
      ) {
        ++counter;
        flag = PlayerSlot.Bottom;
      }

      if (flag != PlayerSlot.None) {
        const chance = 1 - (counter - 3) / 5;
        if (Math.random() >= chance) {
          this.storage.setItem("rinolin", "1");
          this.createPlayerButton(0, 0, this.playerSpecs["rin"]);
          this.movePlayer("rin", flag);

          if (flag == PlayerSlot.Bottom) {
            bottomPlayer = this.finalizePlayer(otherPlayers, flag);
          } else {
            topPlayer = this.finalizePlayer(otherPlayers, flag);
          }
        }
        this.storage.setItem("counter", `${counter}`);
      }
    }

    const cpu = [bottomPlayer, topPlayer, middlePlayer].map(ps => {
      return ps?.id == "pri";
    }, this);

    // relevant dialogs
    let dialogs = [this.findDialog(bottomPlayer, topPlayer)];
    if (middlePlayer) {
      // add convos with middle player and shuffle
      dialogs = dialogs.concat([
        this.findDialog(bottomPlayer, middlePlayer),
        this.findDialog(topPlayer, middlePlayer)
      ]);
      shuffle(dialogs);
      // add the trio dialogue, with preference
      dialogs.unshift(this.findDialog(bottomPlayer, topPlayer, middlePlayer));
    }

    dialogs = dialogs
      .filter(d => d)
      .map(d => {
        // removes past seen dialogues
        const index = parseInt(this.storage.getItem(`d:${d.id}`));
        if (index != 0) {
          d.convos = d.convos.slice(index);
        }

        return d;
      })
      .filter(d => {
        return d.convos.length != 0;
      });

    const highlight = !!parseInt(this.storage.getItem("h"));

    return {
      layout: this.boardLayout,
      bottomPlayer: bottomPlayer,
      topPlayer: topPlayer,
      middlePlayer: middlePlayer,
      cpu: cpu,
      dialogs: dialogs,
      highlight: highlight
    };
  }

  private state: MenuState;

  transitionTo(state: MenuState): void {
    // console.log(`Context: Transition to ${(state as any).constructor.name}.`);
    this.state = state;
    this.state.setScene(this);
  }

  onGamemodePointerUp(layout: BoardLayout): void {
    this.state.onGamemodePointerUp(layout);
  }

  onPlayerPointerDown(player: GameObjects.Container): void {
    this.state.onPlayerPointerDown(player);
  }

  onPlayerPointerUp(player: GameObjects.Container): void {
    this.state.onPlayerPointerUp(player);
  }

  onPlayerDragStart(player: GameObjects.Container): void {
    this.state.onPlayerDragStart(player);
  }

  onPlayerDragEnter(
    player: GameObjects.Container,
    _pointer: Input.Pointer,
    target: GameObjects.GameObject
  ): void {
    this.state.onPlayerDragEnter(player, target);
  }

  onPlayerDrag(
    player: GameObjects.Container,
    pointer: Input.Pointer,
    x: number,
    y: number
  ): void {
    this.state.onPlayerDrag(player, pointer, x, y);
  }

  onPlayerDragLeave(
    player: GameObjects.Container,
    _pointer: Input.Pointer,
    target: GameObjects.GameObject
  ): void {
    this.state.onPlayerDragLeave(player, target);
  }

  onPlayerDragEnd(player: GameObjects.Container): void {
    this.state.onPlayerDragEnd(player);
  }

  onColorBasePointerDown(button: GameObjects.Container): void {
    this.state.onColorBasePointerDown(button);
  }

  onColorRibbonPointerDown(button: GameObjects.Container): void {
    this.state.onColorRibbonPointerDown(button);
  }

  onColorResetPointerDown(button: GameObjects.Container): void {
    this.state.onColorResetPointerDown(button);
  }

  onSliderPointerDown(
    slider: GameObjects.Sprite,
    mask: ColorMask,
    pointer: Phaser.Input.Pointer
  ): void {
    this.state.onSliderPointerDown(slider, mask, pointer);
  }
}

/* eslint-disable @typescript-eslint/no-empty-function  */
/* eslint  @typescript-eslint/no-use-before-define: ["error", { "classes": false }] */
abstract class MenuState {
  protected scene: MenuScene;

  setScene(scene: MenuScene): void {
    this.scene = scene;
  }

  abstract onGamemodePointerUp(layout: BoardLayout): void;

  abstract onPlayerPointerDown(player: GameObjects.Container): void;
  abstract onPlayerPointerUp(player: GameObjects.Container): void;

  abstract onPlayerDragStart(player: GameObjects.Container): void;
  abstract onPlayerDragEnter(
    player: GameObjects.Container,
    target: GameObjects.GameObject
  ): void;
  abstract onPlayerDrag(
    player: GameObjects.Container,
    pointer: Input.Pointer,
    x: number,
    y: number
  ): void;
  abstract onPlayerDragLeave(
    player: GameObjects.Container,
    target: GameObjects.GameObject
  ): void;
  abstract onPlayerDragEnd(player: GameObjects.Container): void;

  abstract onColorBasePointerDown(player: GameObjects.Container): void;
  abstract onColorRibbonPointerDown(player: GameObjects.Container): void;
  abstract onColorResetPointerDown(player: GameObjects.Container): void;
  abstract onSliderPointerDown(
    slider: GameObjects.Sprite,
    mask: ColorMask,
    pointer: Phaser.Input.Pointer
  ): void;
}

class NeutralState extends MenuState {
  instantSelection: boolean;

  constructor() {
    super();
    this.instantSelection = false;
  }

  onGamemodePointerUp(layout: BoardLayout): void {
    this.scene.setBoardType(layout);
  }

  onPlayerPointerDown(_player: GameObjects.Container): void {
    this.instantSelection = true;
  }

  onPlayerPointerUp(player: GameObjects.Container): void {
    const slot = player.state as PlayerSlot;
    if (this.instantSelection && slot == PlayerSlot.None) {
      this.instantSelection = false;
      // immediately move unassigned players up if there's an empty slot
      const emptySlot = this.scene.getFirstEmptySlot();
      if (emptySlot != PlayerSlot.None) {
        this.scene.movePlayer(player, emptySlot);
        return;
      }
    }
    // else select the player
    (player.list[1] as GameObjects.Image).setTexture("menu:playerSelected");
    if (slot == PlayerSlot.None) {
      this.scene.transitionTo(new SelectedUnassignedPlayerState(player));
    } else {
      this.scene.transitionTo(new SelectedChosenPlayerState(player));
    }
  }

  onPlayerDragStart(player: GameObjects.Container): void {
    player.setDepth(dragDepth);
    this.scene.transitionTo(new DraggingPlayerState());
  }

  onPlayerDragEnter(
    _player: GameObjects.Container,
    _target: GameObjects.GameObject
  ): void {}
  onPlayerDrag(
    _player: GameObjects.Container,
    _pointer: Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onPlayerDragLeave(
    _player: GameObjects.Container,
    _target: GameObjects.GameObject
  ): void {}
  onPlayerDragEnd(_player: GameObjects.Container): void {}

  onColorBasePointerDown(): void {}
  onColorRibbonPointerDown(): void {}
  onColorResetPointerDown(): void {}
  onSliderPointerDown(): void {}
}

abstract class SelectedPlayerState extends MenuState {
  protected player: GameObjects.Container;
  protected playerData: PlayerScaffold;
  protected sliderSubState: boolean;
  protected slider: GameObjects.Sprite;
  protected target: CustomColor;
  protected colorMask: ColorMask;

  constructor(player: GameObjects.Container) {
    super();
    this.player = player;
    this.playerData = player.data.values.player;
    this.sliderSubState = false;
  }

  setScene(scene: MenuScene): void {
    super.setScene(scene);
    this.target = this.scene.enableCustomColors(this.playerData);
  }

  protected deselect(): void {
    (this.player.list[1] as GameObjects.Image).setTexture("menu:playerBorder");
    this.scene.disableCustomColors();
  }

  protected updateColor(pointer: Phaser.Input.Pointer): void {
    const minX = this.slider.getLeftCenter().x + this.slider.parentContainer.x;
    const baseColor = this.playerData.base ? this.playerData.base : 0xffffff;
    const ribbonColor = this.playerData.ribbon
      ? this.playerData.ribbon
      : 0xffffff;

    let x = (pointer.worldX - minX) / this.slider.width;
    x = x < 0 ? 0 : x;
    x = x > 1 ? 1 : x;

    const mask = this.colorMask;
    let color: number;
    switch (this.target) {
      case CustomColor.Base:
      case CustomColor.OnlyBase:
        color = (baseColor & ~mask) | ((x * mask) & mask);
        this.playerData.base = color;
        (this.player.first as GameObjects.Image).setTint(color);
        break;
      case CustomColor.Ribbon:
      case CustomColor.OnlyRibbon:
        color = (ribbonColor & ~mask) | ((x * mask) & mask);
        this.playerData.ribbon = color;
        (this.player.list[1] as GameObjects.Image).setTint(color);
        break;
    }

    this.scene.updateCustomColors(this.playerData, this.target);
  }

  onGamemodePointerUp(type: BoardLayout): void {
    // for extra safety, bc this shouldn't fire anyway, we only do this outside slider substate
    if (!this.sliderSubState) {
      // for safety we always "deselect" and then do the game mode swap
      this.deselect();
      this.scene.transitionTo(new NeutralState());
      this.scene.setBoardType(type);
    }
  }

  onPlayerDragStart(player: GameObjects.Container): void {
    this.deselect();

    player.setDepth(dragDepth);
    this.scene.transitionTo(new DraggingPlayerState());
  }

  onPlayerDragEnter(
    _player: GameObjects.Container,
    _target: GameObjects.GameObject
  ): void {}
  onPlayerDrag(
    _player: GameObjects.Container,
    _pointer: Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onPlayerDragLeave(
    _player: GameObjects.Container,
    _target: GameObjects.GameObject
  ): void {}
  onPlayerDragEnd(_player: GameObjects.Container): void {}

  onColorBasePointerDown(_button: GameObjects.Container): void {
    this.target = this.scene.enableCustomColors(
      this.playerData,
      CustomColor.Base
    );
  }

  onColorRibbonPointerDown(_button: GameObjects.Container): void {
    this.target = this.scene.enableCustomColors(
      this.playerData,
      CustomColor.Ribbon
    );
  }

  onColorResetPointerDown(_button: GameObjects.Container): void {
    this.scene.storage.removeItem(`pb:${this.player.name}`);
    this.scene.storage.removeItem(`pr:${this.player.name}`);

    if (this.target != CustomColor.OnlyRibbon) {
      this.playerData.base = this.player.data.values.originalBase;
      (this.player.first as GameObjects.Image).setTint(this.playerData.base);
    }
    if (this.target != CustomColor.OnlyBase) {
      this.playerData.ribbon = this.player.data.values.originalRibbon;
      (this.player.list[1] as GameObjects.Image).setTint(
        this.playerData.ribbon
      );
    }

    this.scene.updateCustomColors(this.playerData, this.target);
  }

  onSliderPointerDown(
    slider: GameObjects.Sprite,
    type: ColorMask,
    pointer: Phaser.Input.Pointer
  ): void {
    this.slider = slider;
    this.colorMask = type;
    this.sliderSubState = true;

    this.scene.input.on("pointermove", this.onPointerMove, this);
    this.scene.input.on("pointerup", this.onPointerUp, this);
    this.updateColor(pointer);
  }

  onPointerMove(pointer: Phaser.Input.Pointer): void {
    this.updateColor(pointer);
  }

  onPointerUp(_pointer: Phaser.Input.Pointer): void {
    this.sliderSubState = false;

    this.scene.input.off("pointermove", this.onPointerMove, this);
    this.scene.input.off("pointerup", this.onPointerUp, this);

    switch (this.target) {
      case CustomColor.Base:
      case CustomColor.OnlyBase:
        this.scene.storage.setItem(
          `pb:${this.player.name}`,
          this.player.data.values.player.base
        );
        break;
      case CustomColor.Ribbon:
      case CustomColor.OnlyRibbon:
        this.scene.storage.setItem(
          `pr:${this.player.name}`,
          this.player.data.values.player.ribbon
        );
        break;
    }
  }
}

class SelectedUnassignedPlayerState extends SelectedPlayerState {
  onPlayerPointerDown(_player: GameObjects.Container): void {}

  onPlayerPointerUp(player: GameObjects.Container): void {
    if (player.state == PlayerSlot.None) {
      // do nothing, works for both a self-click and when targetting another unselected
    } else {
      // swap this one out, works for both chosen and unnassigned
      this.scene.movePlayer(this.player, player.state as PlayerSlot);
    }

    this.deselect();
    this.scene.transitionTo(new NeutralState());
  }
}

class SelectedChosenPlayerState extends SelectedPlayerState {
  protected updateColor(pointer: Phaser.Input.Pointer): void {
    super.updateColor(pointer);
    this.scene.updateRules(this.player.state as PlayerSlot, this.target);
  }

  onPlayerPointerDown(_player: GameObjects.Container): void {}

  onPlayerPointerUp(player: GameObjects.Container): void {
    if (this.player == player) {
      // remove
      this.scene.clearSlot(this.player.state as PlayerSlot);
    } else {
      // swap this one out, works for both chosen and unnassigned
      this.scene.movePlayer(player, this.player.state as PlayerSlot);
    }

    this.deselect();
    this.scene.transitionTo(new NeutralState());
  }

  onColorResetPointerDown(button: GameObjects.Container): void {
    super.onColorResetPointerDown(button);
    this.scene.updateRules(this.player.state as PlayerSlot, this.target);
  }
}

class DraggingPlayerState extends MenuState {
  protected slotTarget: GameObjects.Image;
  protected playerTarget: GameObjects.Image;

  onGamemodePointerUp(_layout: BoardLayout): void {}

  onPlayerPointerDown(_player: GameObjects.Container): void {}
  onPlayerPointerUp(_player: GameObjects.Container): void {
    // this avoids the stone starting selected after a drag, is this intended?
    this.scene.transitionTo(new NeutralState());
  }
  onPlayerDragStart(_player: GameObjects.Container): void {}

  onPlayerDragEnter(
    _player: GameObjects.Container,
    target: GameObjects.GameObject
  ): void {
    if (target.name.startsWith("slot")) {
      this.slotTarget = target as GameObjects.Image;
    } else {
      this.playerTarget = target as GameObjects.Image;
    }
  }

  onPlayerDrag(
    player: GameObjects.Container,
    _pointer: Input.Pointer,
    x: number,
    y: number
  ): void {
    player.setX(x).setY(y);
  }

  onPlayerDragLeave(
    _player: GameObjects.Container,
    target: GameObjects.GameObject
  ): void {
    if (target.name.startsWith("slot")) {
      this.slotTarget = undefined;
    } else {
      this.playerTarget = undefined;
    }
  }

  onPlayerDragEnd(player: GameObjects.Container): void {
    player.setDepth(playerDepth);

    if (this.slotTarget) {
      this.scene.movePlayer(player, this.slotTarget.state as PlayerSlot);
    } else if (this.playerTarget && player.state != PlayerSlot.None) {
      this.scene.movePlayer(this.playerTarget.name, player.state as PlayerSlot);
    } else {
      this.scene.movePlayer(player, PlayerSlot.None);
    }
  }

  onColorBasePointerDown(_player: GameObjects.Container): void {}
  onColorRibbonPointerDown(_player: GameObjects.Container): void {}
  onColorResetPointerDown(_player: GameObjects.Container): void {}
  onSliderPointerDown(
    _slider: GameObjects.Sprite,
    _mask: ColorMask,
    _pointer: Input.Pointer
  ): void {}
}
