import "phaser";
import {
  extendHex,
  defineGrid,
  Hex,
  Grid,
  HexFactory,
  GridFactory
} from "honeycomb-grid";
import { FlatCompassDirection } from "../specs";
import { PlayerHeight } from "../specs";
import { NeutralState, BoardState } from "./boardState";
import {
  HexTile,
  TileState,
  getTopTile,
  BoardSceneInterface,
  Moves,
  getTileHeight,
  BoardData
} from "./boardSpecs";
import { Player } from "./player";
import { GameObjects, Input } from "phaser";

export abstract class Board {
  Hex: HexFactory<HexTile>;
  Grid: GridFactory<Hex<HexTile>>;
  scene: Phaser.Scene & BoardSceneInterface;
  x: number;
  y: number;
  depth: number;
  radius: number;
  unit: number;
  corners: { x: number; y: number }[];

  grid: Grid<Hex<HexTile>>;
  teleporters: Hex<HexTile>[];
  players: Player[];
  nrPlayers: number;
  edges: GameObjects.Container[];

  private state: BoardState;

  currentPlayer: Player;
  currentMoves: Moves[];
  currentThreats: Set<Hex<HexTile>>;

  constructor({
    scene: scene,
    x: x,
    y: y,
    depth: depth,
    players,
    ai: ai
  }: BoardData) {
    this.scene = scene;
    this.x = x;
    this.y = y;
    this.depth = depth;
    this.players = players
      .filter(ps => ps)
      .map((ps, i) => {
        const player = new Player(ps);
        if (ai[i]) {
          player.artificialize(this);
        }
        return player;
      }, this);
    this.nrPlayers = this.players.length;
    this.edges = [];

    // hardcoded values for boardsize 3
    const radius = 3;
    this.radius = radius;
    this.unit = 96;
    this.corners = [
      { x: -2, y: -3 },
      { x: 2, y: -3 },
      { x: 4, y: 0 },
      { x: 2, y: 3 },
      { x: -2, y: 3 },
      { x: -4, y: 0 }
    ].map(p => {
      return { x: p.x * this.unit, y: p.y * this.unit };
    });

    // create hexagon grid
    // board (using Hex grid)
    this.Hex = extendHex({
      size: { width: this.unit, height: this.unit },
      stack: undefined,
      stone: undefined
    });
    this.Grid = defineGrid(this.Hex);

    this.setupBoard();
    this.generateBoard();

    this.currentPlayer = this.players[0];
    this.currentMoves = [];
    this.transitionTo(new NeutralState());
  }

  public isEdge(hex: Hex<HexTile>, player: Player): boolean {
    switch (player.edgeDirection) {
      case FlatCompassDirection.SE:
        return -hex.s >= this.radius;

      case FlatCompassDirection.S:
        return hex.r >= this.radius;

      case FlatCompassDirection.SW:
        return -hex.q >= this.radius;

      case FlatCompassDirection.NW:
        return hex.s >= this.radius;

      case FlatCompassDirection.N:
        return -hex.r >= this.radius;

      case FlatCompassDirection.NE:
        return hex.q >= this.radius;
    }
  }

  public getNeighbours(hex: Hex<HexTile>, height: TileState): Hex<HexTile>[] {
    return (
      this.grid
        .neighborsOf(hex)
        // filters out the unassigned neigbours right away
        .filter(h => h)
        .map(h => {
          if (h.referral) return h.referral;
          else return h;
        })
        .filter(h => {
          if (!h.stone)
            return !h.stone && h.stack.length == getTileHeight(height);
        })
    );
  }

  public countStoneThreats(hex: Hex<HexTile>, player: Player): number {
    return (
      this.grid
        .neighborsOf(hex)
        // filters out the unassigned neigbours right away
        .filter(h => h)
        .map(h => {
          if (h.referral) return h.referral;
          else return h;
        })
        .filter(h => {
          return h.stone && h.stone.data.values.player == player;
        }).length
    );
  }

  private updateTileTexture(tile: GameObjects.Sprite): void {
    const state = tile.state as TileState;
    if (state & TileState.Rise) {
      tile.setTexture("board:tileRise").disableInteractive();
      return;
    }
    const height = getTileHeight(state);
    let texture = "board:tile";
    switch (height) {
      case TileState.Bottom:
        texture += "Bottom";
        break;
      case TileState.Middle:
        texture += "Middle";
        break;
      case TileState.Top:
        texture += "Top";
        break;
    }

    if (this.scene.isHighlighted()) {
      if (state & TileState.Selected || state & TileState.Source) {
        tile.setTexture(texture + "Source");
        return;
      } else if (state & TileState.Target) {
        tile.setTexture(texture + "Target");
        return;
      } else if (state & TileState.Capture) {
        tile.setTexture(texture + "Capture");
        return;
      }
    } else if (state & TileState.Selected) {
      tile.setTexture(texture + "Selected");
      return;
    }

    tile.setTexture(texture);
  }

  public setTileState(
    tile: GameObjects.Sprite,
    state?: TileState
  ): GameObjects.Sprite {
    if (!state) {
      state = tile.data.values.hex.stack.length as TileState;
    } else {
      state = (state & ~0b11) | ((tile.state as TileState) & 0b11);
    }
    tile.setState(state);
    this.updateTileTexture(tile);
    return tile;
  }

  private setHeightedTileState(
    tile: GameObjects.Sprite,
    state: TileState,
    height: TileState
  ): GameObjects.Sprite {
    tile.setState((state & ~0b11) | height);
    this.updateTileTexture(tile);
    return tile;
  }

  protected createEdge(
    x: number,
    y: number,
    side: string,
    flip = false,
    player?: Player
  ): void {
    const edgeRibbon = this.scene.add
      .image(0, 0, `board:border_${side}Off`)
      .setFlipX(flip);
    const edgeBody = this.scene.add
      .image(0, 0, `board:border_${side}Ribbon`)
      .setVisible(false)
      .setFlipX(flip);

    const edge = this.scene.add.container(x, y, [edgeRibbon, edgeBody]);
    if (player) {
      edgeRibbon.setTexture(`board:border_${side}On`).setTint(player.ribbon);
      edgeBody.setVisible(true).setTint(player.base);
      player.edge = edge;
    }
  }

  protected abstract setupBoard(): void;

  private generateBoard(): void {
    // background
    const physicalEdges = this.corners.map(p => {
      return { x: p.x + this.x, y: p.y + this.y };
    });

    this.scene.add
      .polygon(0, 0, physicalEdges, 0x917c6f)
      .setOrigin(0, 0)
      .setStrokeStyle(1, 0x483e37);

    this.generateEdges(physicalEdges);

    // stacks (tiles & stones)
    this.grid = this.Grid.spiral({ radius: this.radius });

    // generate the tiles
    this.generateTiles();

    // teleporter spots
    this.generateTeleports();

    this.teleporters.forEach(hex => {
      this.grid.push(hex);
    });

    // set everything's interactivity
    this.setSourceInteractivity();
  }

  protected abstract generateEdges(
    physicalEdges: { x: number; y: number }[]
  ): void;

  protected abstract generateTiles(): void;

  protected abstract generateTeleports(): void;

  protected addTile(hex: Hex<HexTile>, tile: GameObjects.Sprite): boolean {
    // can't add a type above a non-tile
    if (hex.stone || hex.stack.length >= 3) {
      return false;
    }

    const coords = hex.toPoint();

    tile.x = this.x + coords.x;
    tile.y = this.y - 8 * hex.stack.length + coords.y;
    tile.setData("hex", hex);
    hex.stack.push(tile);

    this.updateHexDepth(hex);

    return true;
  }

  public updateHexDepth(hex: Hex<HexTile>): void {
    const rowDepth = this.depth + (this.radius + hex.r) * 6;

    hex.stack.forEach((tile, i) => {
      const height = (i + 1) as TileState;
      tile.depth = rowDepth + i * 2;
      if (hex.stack.length != 1 && height != hex.stack.length) {
        this.setHeightedTileState(tile, TileState.Rise, height);
        tile.depth += 1;
      } else {
        // hacky way but this clears both the rising flag and the height in one go
        // but preserves the other flags, just in case
        this.setHeightedTileState(
          tile,
          (tile.state as TileState) & ~TileState.Rise,
          height
        );
      }
    });
    if (hex.stone) {
      const tile = getTopTile(hex);
      tile.disableInteractive();
      hex.stone.depth = tile.depth + 1;
    }
  }

  protected addStone(hex: Hex<HexTile>, stone: GameObjects.Container): boolean {
    // can't add a type above a non-tile
    if (hex.stone) {
      return false;
    }

    const coords = hex.toPoint();
    const tile = getTopTile(hex).disableInteractive();

    stone.x = this.x + coords.x;
    stone.y = this.y + -8 * (hex.stack.length - 1) + coords.y;
    stone.setData("hex", hex);
    stone.depth = tile.depth + 1;
    hex.stone = stone;

    return true;
  }

  resetStone(target: Hex<HexTile>): void {
    const coords = target.toPoint();
    const stone = target.stone;

    stone.x = this.x + coords.x;
    stone.y = this.y + -8 * (target.stack.length - 1) + coords.y;
    stone.depth = target.stack[target.stack.length - 1].depth + 1;
  }

  resetTile(target: Hex<HexTile>): void {
    const coords = target.toPoint();
    const tile = target.stack[target.stack.length - 1];

    tile.x = this.x + coords.x;
    tile.y = this.y - 8 * target.stack.length + coords.y;

    this.updateHexDepth(target);
  }

  moveTile(origin: Hex<HexTile>, target: Hex<HexTile>): void {
    if (origin.stack.length <= 1) return;

    const tile = origin.stack.pop();
    if (!this.addTile(target, tile)) {
      origin.stack.push(tile);
    } else {
      this.updateHexDepth(origin);
    }
  }

  moveStone(origin: Hex<HexTile>, target: Hex<HexTile>): void {
    const stone = origin.stone;
    if (this.addStone(target, stone)) {
      origin.stone = undefined;
    }
  }

  protected createTile(hex: Hex<HexTile>): GameObjects.Sprite {
    if (hex.stone) return;

    const corners = hex.corners().map(p => {
      return new Phaser.Geom.Point(p.x, p.y);
    });

    const tile = this.scene.add
      // length = stack size
      .sprite(0, 0, "tileBottom")
      .setName("tile")
      .setInteractive({
        hitArea: new Phaser.Geom.Polygon(corners),
        hitAreaCallback: Phaser.Geom.Polygon.Contains,
        useHandCursor: true,
        dropZone: true
      })
      .disableInteractive();

    tile.on("pointerdown", this.onTilePointerDown.bind(this, tile));
    tile.on("pointerover", this.onTilePointerOver.bind(this, tile));
    tile.on("pointerout", this.onTilePointerOut.bind(this, tile));
    tile.on("pointerup", this.onTilePointerUp.bind(this, tile));

    tile.on("dragstart", this.onTileDragStart.bind(this, tile));
    tile.on("dragenter", this.onTileDragEnter.bind(this, tile));
    tile.on("drag", this.onTileDrag.bind(this, tile));
    tile.on("dragleave", this.onTileDragLeave.bind(this, tile));
    tile.on("dragend", this.onTileDragEnd.bind(this, tile));

    this.scene.input.setDraggable(tile);

    this.addTile(hex, tile);
    return tile;
  }

  protected createStone(
    hex: Hex<HexTile>,
    player: Player
  ): GameObjects.Container {
    // todo - make this into a single entity with a baked texture?
    const base = this.scene.add.sprite(0, 0, "board:stoneBase");

    const ribbon = this.scene.add.sprite(0, 0, "board:stoneRibbon");
    if (player) {
      base.setTint(player.base);
      ribbon.setTint(player.ribbon);
    }

    const stone = this.scene.add.container(0, 0, [base, ribbon]);
    stone
      .setName("stone")
      .setData("player", player)
      .setInteractive({
        hitArea: new Phaser.Geom.Ellipse(
          stone.x,
          stone.y,
          base.width,
          base.height
        ),
        hitAreaCallback: Phaser.Geom.Ellipse.Contains,
        useHandCursor: true,
        draggable: true
      })
      .disableInteractive();

    stone.on("pointerdown", this.onStonePointerDown.bind(this, stone));
    stone.on("pointerover", this.onStonePointerOver.bind(this, stone));
    stone.on("pointerout", this.onStonePointerOut.bind(this, stone));
    stone.on("pointerup", this.onStonePointerUp.bind(this, stone));

    stone.on("dragstart", this.onStoneDragStart.bind(this, stone));
    stone.on("dragenter", this.onStoneDragEnter.bind(this, stone));
    stone.on("drag", this.onStoneDrag.bind(this, stone));
    stone.on("dragleave", this.onStoneDragLeave.bind(this, stone));
    stone.on("dragend", this.onStoneDragEnd.bind(this, stone));

    this.addStone(hex, stone);
    player.stones.add(stone);
    return stone;
  }

  destroyStone(stone: GameObjects.Container, callback?: Function): void {
    (stone.first as GameObjects.Sprite).setTint(0x696969);
    (stone.last as GameObjects.Sprite).setTint(0x111111);
    stone.disableInteractive();

    // make this a smooth clean, so we need to remove references to:
    //   player
    const player = stone.data.values.player as Player;
    player.stones.delete(stone);
    //   hex
    const hex = stone.data.values.hex as Hex<HexTile>;
    hex.stone = undefined;

    this.scene.tweens.add({
      targets: stone,
      alpha: { from: 1, to: 0 },
      onComplete: callback ?? stone.destroy.bind(stone)
    });
  }

  // interaction changes
  disableBoard(): void {
    this.grid
      .filter(hex => hex.stack)
      .forEach(hex => {
        this.setTileState(getTopTile(hex)).disableInteractive();
        if (hex.stone) hex.stone.disableInteractive();
      }, this);
  }

  toggleHighlight(): void {
    this.grid.forEach(hex => {
      if (hex.referral) return;
      this.updateTileTexture(getTopTile(hex));
    }, this);
  }

  setNextPlayer(): void {
    // invalid number of moves, can't continue with this player
    const players = this.players;
    const playerIndex =
      (players.indexOf(this.currentPlayer) + 1) % players.length;

    this.currentPlayer = players[playerIndex];
    this.currentMoves = [];
    this.currentThreats = new Set<Hex<HexTile>>();

    this.scene.newTurn(this.currentPlayer.height);
    if (playerIndex == 0) {
      this.scene.newRound();
    }
  }

  setSourceInteractivity(
    simulation?: boolean
  ): {
    tiles: Set<Hex<HexTile>>;
    stones: Set<Hex<HexTile>>;
    captures: Set<Hex<HexTile>>;
    count: number;
    valid: boolean;
  } {
    const blockedStones = new Set<Hex<HexTile>>();
    const stones = new Set<Hex<HexTile>>();
    const tiles = new Set<Hex<HexTile>>();
    const captures = new Set<Hex<HexTile>>();

    if (!this.currentPlayer || this.currentMoves.length >= 2) {
      return {
        tiles: tiles,
        stones: stones,
        captures: captures,
        count: 0,
        valid: false
      };
    }

    if (!simulation) this.disableBoard();

    // if this is the first move, and only then, we generate captures from threats
    if (this.currentMoves.length == 0) {
      this.currentThreats = new Set<Hex<HexTile>>();

      this.players.forEach(player => {
        if (player != this.currentPlayer) {
          player.stones.forEach(stone => {
            const hex = stone.data.values.hex;
            // count the number of player stones around this enemy stone
            const threats = this.countStoneThreats(hex, this.currentPlayer);
            if (threats >= 2) {
              // this stone can be captured!
              this.currentThreats.add(hex);
            }
          });
        }
      }, this);
    }

    if (this.currentMoves.indexOf(Moves.Capture) < 0) {
      // on moves 1+, the targets are locked
      this.currentThreats.forEach(hex => {
        captures.add(hex);
      });
    }

    // let's now look at the current player's stone for possible moves
    if (this.currentMoves.indexOf(Moves.Stone) < 0) {
      this.currentPlayer.stones.forEach(stone => {
        const hex = stone.data.values.hex;
        // add it as interactable if it has neigbour tiles it can move it
        const neighbours = this.getNeighbours(hex, hex.stack.length);
        if (neighbours.length > 0) {
          stones.add(hex);
        } else {
          blockedStones.add(hex);
        }
      }, this);
    }

    // now time to check the movable tiles
    if (this.currentMoves.indexOf(Moves.Tile) < 0) {
      // now, if we're a bottom-player, we might be stuck as we can't allow
      //  non-neighbour tile moves if we could but can't move our own pieces (can move stones, none to move)
      if (
        this.currentPlayer.height == PlayerHeight.Bottom &&
        this.currentMoves.indexOf(Moves.Stone) < 0 &&
        stones.size == 0
      ) {
        // get all uncovered middle-height tiles around stones, and make them interactive
        blockedStones.forEach(blockedHex => {
          this.getNeighbours(blockedHex, TileState.Middle)
            .filter(hex => {
              return !this.isEdge(hex, this.currentPlayer);
            })
            .forEach(hex => {
              tiles.add(hex);
            });
        });
      } else {
        // we set tiles as interactable as it's usual
        this.grid.forEach(hex => {
          // let's avoid the teleporting hexes, to avoid double-interaction sets
          if (hex.referral || hex.stone || hex.stack.length == TileState.Bottom)
            return;

          if (!this.isEdge(hex, this.currentPlayer)) {
            tiles.add(hex);
          }
        });
      }
    }

    if (!simulation) {
      tiles.forEach(hex => {
        this.setTileState(getTopTile(hex).setInteractive(), TileState.Source);
      }, this);
      stones.forEach(hex => {
        const tile = getTopTile(hex);
        hex.stone.setInteractive();
        this.setTileState(tile, TileState.Source);
      }, this);
      captures.forEach(hex => {
        const tile = getTopTile(hex);
        hex.stone.setInteractive();
        this.setTileState(tile, TileState.Capture);
      }, this);
    }

    const count = tiles.size + stones.size + captures.size;
    return {
      tiles: tiles,
      stones: stones,
      captures: captures,
      count: count,
      valid: true
    };
  }

  setTileTargetInteractivity(
    tile: GameObjects.Sprite,
    simulation?: boolean
  ): Set<Hex<HexTile>> {
    const sourceHex = tile.data.values.hex;

    if (!simulation) {
      this.disableBoard();
      this.setTileState(
        getTopTile(sourceHex).setInteractive(),
        TileState.Selected
      );
    }

    const filterSource = function(hex: Hex<HexTile>): boolean {
      return hex != sourceHex;
    };

    let hasStoneMoves = false;
    const blockedStones = new Set<Hex<HexTile>>();
    const tiles = new Set<Hex<HexTile>>();
    // first, let's look for stones
    this.currentPlayer.stones.forEach(stone => {
      // friendly stone, check for neighbours
      const hex = stone.data.values.hex;
      if (this.currentMoves.indexOf(Moves.Stone) < 0) {
        // add it as interactable if it has neigbour tiles it can move it
        const neighbours = this.getNeighbours(hex, hex.stack.length).filter(
          filterSource
        );
        if (neighbours.length > 0) {
          hasStoneMoves = true;
        } else {
          blockedStones.add(hex);
        }
      }
    });

    // now, if we're a top-player, we might be stuck as we can't allow to place anywhere but on
    //  neighbour tile if we could but can't move our own pieces (can move stones, none to move)
    if (
      !hasStoneMoves &&
      this.currentPlayer.height == PlayerHeight.Top &&
      this.currentMoves.indexOf(Moves.Stone) < 0
    ) {
      // get all uncovered middle-height tiles around stones, and make them interactive
      blockedStones.forEach(blockedHex => {
        this.getNeighbours(blockedHex, TileState.Middle)
          .filter(hex => {
            return filterSource(hex) && !this.isEdge(hex, this.currentPlayer);
          })
          .forEach(hex => {
            tiles.add(hex);
          });
      });
    } else {
      // we can set all (filtered before) tiles as interactable
      this.grid.forEach(hex => {
        // let's avoid the teleporting hexes, to avoid double-interaction sets
        if (hex.referral || hex.stone || hex.stack.length == TileState.Top)
          return;
        // let's just store tiles for now, if they're the correct height
        if (filterSource(hex) && !this.isEdge(hex, this.currentPlayer)) {
          tiles.add(hex);
        }
      });
    }

    if (!simulation) {
      tiles.forEach(hex => {
        this.setTileState(getTopTile(hex).setInteractive(), TileState.Target);
      }, this);
    }

    return tiles;
  }

  setStoneTargetInteractivity(
    stone: GameObjects.Container,
    simulation?: boolean
  ): Set<Hex<HexTile>> {
    if (!simulation) {
      this.disableBoard();
      // we're assuming stones targetted by this always had targets to begin with
      stone.setInteractive();
      this.setTileState(getTopTile(stone.data.values.hex), TileState.Selected);
    }

    const tiles = new Set<Hex<HexTile>>();
    // stones can never lead to impossible moves, they're outnumbered by tiles
    const stoneHex = stone.data.values.hex;
    this.getNeighbours(stoneHex, stoneHex.stack.length).forEach(hex => {
      if (!simulation) {
        this.setTileState(getTopTile(hex).setInteractive(), TileState.Target);
      }
      tiles.add(hex);
    }, this);

    return tiles;
  }

  lose(player: Player): void {
    (player.edge?.first as GameObjects.Image)?.setTint(0x3f3f3f);
    (player.edge?.last as GameObjects.Image)?.setTint(0x696969);

    player.lost = true;

    player.stones.forEach(stone => {
      this.destroyStone(stone);
    });

    this.scene.lost(player.height);
  }

  // state change triggers

  public transitionTo(state: BoardState): void {
    // console.log(
    //   `Context: Transition to ${(state as BoardState).constructor.name}.`
    // );
    this.state = state;
    this.state.setBoard(this);
  }

  onTilePointerDown(tile: GameObjects.Sprite): void {
    this.state.onTilePointerDown(tile);
  }
  onTilePointerOver(tile: GameObjects.Sprite): void {
    this.state.onTilePointerOver(tile);
  }
  onTilePointerOut(tile: GameObjects.Sprite): void {
    this.state.onTilePointerOut(tile);
  }
  onTilePointerUp(tile: GameObjects.Sprite): void {
    this.state.onTilePointerUp(tile);
  }

  onTileDragStart(tile: GameObjects.Sprite): void {
    this.state.onTileDragStart(tile);
  }
  onTileDragEnter(
    tile: GameObjects.Sprite,
    _pointer: Input.Pointer,
    target: GameObjects.GameObject
  ): void {
    this.state.onTileDragEnter(tile, target);
  }
  onTileDrag(
    tile: GameObjects.Sprite,
    pointer: Input.Pointer,
    x: number,
    y: number
  ): void {
    this.state.onTileDrag(tile, pointer, x, y);
  }
  onTileDragLeave(
    tile: GameObjects.Sprite,
    _pointer: Input.Pointer,
    target: GameObjects.GameObject
  ): void {
    this.state.onTileDragLeave(tile, target);
  }
  onTileDragEnd(tile: GameObjects.Sprite): void {
    this.state.onTileDragEnd(tile);
  }
  onStonePointerDown(stone: GameObjects.Container): void {
    this.state.onStonePointerDown(stone);
  }
  onStonePointerOver(stone: GameObjects.Container): void {
    this.state.onStonePointerOver(stone);
  }
  onStonePointerOut(stone: GameObjects.Container): void {
    this.state.onStonePointerOut(stone);
  }
  onStonePointerUp(stone: GameObjects.Container): void {
    this.state.onStonePointerUp(stone);
  }

  onStoneDragStart(stone: GameObjects.Container): void {
    this.state.onStoneDragStart(stone);
  }
  onStoneDragEnter(
    stone: GameObjects.Container,
    _pointer: Phaser.Input.Pointer,
    target: GameObjects.GameObject
  ): void {
    this.state.onStoneDragEnter(stone, target);
  }
  onStoneDrag(
    stone: GameObjects.Container,
    pointer: Phaser.Input.Pointer,
    x: number,
    y: number
  ): void {
    this.state.onStoneDrag(stone, pointer, x, y);
  }
  onStoneDragLeave(
    stone: GameObjects.Container,
    _pointer: Phaser.Input.Pointer,
    target: GameObjects.GameObject
  ): void {
    this.state.onStoneDragLeave(stone, target);
  }
  onStoneDragEnd(stone: GameObjects.Container): void {
    this.state.onStoneDragEnd(stone);
  }
}

export class BoardDuo8 extends Board {
  protected setupBoard(): void {
    const bottomPlayer = this.players[PlayerHeight.Bottom];
    const topPlayer = this.players[PlayerHeight.Top];

    bottomPlayer.height = PlayerHeight.Bottom;
    bottomPlayer.edgeDirection = FlatCompassDirection.SW;

    topPlayer.height = PlayerHeight.Top;
    topPlayer.edgeDirection = FlatCompassDirection.NE;
  }

  protected generateEdges(physicalEdges): void {
    // edge ridges
    const physicalCenters = physicalEdges.map((p, i) => {
      {
        const q = physicalEdges[(i + 1) % 6];
        return { x: (p.x + q.x) / 2, y: (p.y + q.y) / 2 };
      }
    });

    this.createEdge(physicalCenters[0].x, physicalCenters[0].y + 8, "n");
    this.createEdge(
      physicalCenters[1].x - 8,
      physicalCenters[1].y - 8,
      "ne",
      false,
      this.players[PlayerHeight.Top]
    );
    this.createEdge(physicalCenters[2].x - 8, physicalCenters[2].y - 4, "se");
    this.createEdge(physicalCenters[3].x, physicalCenters[3].y - 20, "s");
    this.createEdge(
      physicalCenters[4].x + 8,
      physicalCenters[4].y - 4,
      "se",
      true,
      this.players[PlayerHeight.Bottom]
    );
    this.createEdge(
      physicalCenters[5].x + 8,
      physicalCenters[5].y - 8,
      "ne",
      true
    );
  }

  protected generateTiles(): void {
    this.grid.forEach(hex => {
      if (hex.distance(this.grid[0]) <= this.radius) {
        hex.stack = new Array<GameObjects.Sprite>();
        this.createTile(hex);
        if (hex.q != -3) {
          this.createTile(hex);
          if (hex.q == 3) {
            this.createTile(hex);
            this.createStone(hex, this.players[PlayerHeight.Top]);
          }
        } else {
          this.createStone(hex, this.players[PlayerHeight.Bottom]);
        }
      }
    });
  }

  protected generateTeleports(): void {
    const teleportSpots = [28, 25, 22, 19, 34, 31];

    this.teleporters = this.corners
      .map(p => {
        // the pointToHex() function considers the center of the hexagon its topleft corner
        return { x: p.x + this.unit / 2, y: p.y + this.unit / 2 };
      })
      .map(p => {
        // turns the edge point coordinates into fresh hexes
        return this.Hex(this.Grid.pointToHex(p));
      })
      .map((hex, i) => {
        // and makes them reference the stack of their teleporting target
        // so essentially, any Phaser function querrying it will think it's the second hex
        hex.referral = this.grid.get(teleportSpots[i]);
        return hex;
      });
  }
}

export class BoardDuo10 extends BoardDuo8 {
  protected generateTiles(): void {
    super.generateTiles();

    const bottomSlots = [this.grid.get(16)];
    const topSlots = [this.grid.get(10)];

    for (let index = 0; index < bottomSlots.length; index++) {
      const bottom = bottomSlots[index];
      const top = topSlots[index];

      this.moveTile(bottom, top);
      this.createStone(bottom, this.players[PlayerHeight.Bottom]);
      this.createStone(top, this.players[PlayerHeight.Top]);
    }
  }
}

export class BoardDuo12 extends BoardDuo8 {
  protected generateTiles(): void {
    super.generateTiles();

    const bottomSlots = [this.grid.get(15), this.grid.get(17)];
    const topSlots = [this.grid.get(9), this.grid.get(11)];

    for (let index = 0; index < bottomSlots.length; index++) {
      const bottom = bottomSlots[index];
      const top = topSlots[index];

      this.moveTile(bottom, top);
      this.createStone(bottom, this.players[PlayerHeight.Bottom]);
      this.createStone(top, this.players[PlayerHeight.Top]);
    }
  }
}

export class BoardTrio12 extends Board {
  protected setupBoard(): void {
    const bottomPlayer = this.players[PlayerHeight.Bottom];
    const topPlayer = this.players[PlayerHeight.Top];
    const middlePlayer = this.players[PlayerHeight.Middle];

    bottomPlayer.height = PlayerHeight.Bottom;
    bottomPlayer.edgeDirection = FlatCompassDirection.SW;

    topPlayer.height = PlayerHeight.Top;
    topPlayer.edgeDirection = FlatCompassDirection.N;

    middlePlayer.height = PlayerHeight.Middle;
    middlePlayer.edgeDirection = FlatCompassDirection.SE;
  }

  // TODO: if we use enums, we could even automate this, seeing we save the edgedirection on the Player
  protected generateEdges(physicalEdges): void {
    // edge ridges
    const physicalCenters = physicalEdges.map((p, i) => {
      {
        const q = physicalEdges[(i + 1) % 6];
        return { x: (p.x + q.x) / 2, y: (p.y + q.y) / 2 };
      }
    });
    this.createEdge(
      physicalCenters[0].x,
      physicalCenters[0].y + 8,
      "n",
      false,
      this.players[1]
    );
    this.createEdge(physicalCenters[1].x - 8, physicalCenters[1].y - 8, "ne");
    this.createEdge(
      physicalCenters[2].x - 8,
      physicalCenters[2].y - 4,
      "se",
      false,
      this.players[2]
    );
    this.createEdge(physicalCenters[3].x, physicalCenters[3].y - 20, "s");
    this.createEdge(
      physicalCenters[4].x + 8,
      physicalCenters[4].y - 4,
      "se",
      true,
      this.players[0]
    );
    this.createEdge(
      physicalCenters[5].x + 8,
      physicalCenters[5].y - 8,
      "ne",
      true
    );
  }

  protected generateTiles(): void {
    this.grid.forEach(hex => {
      if (hex.distance(this.grid[0]) <= this.radius) {
        hex.stack = new Array<GameObjects.Sprite>();
        this.createTile(hex);
        if (hex.q != -3) {
          this.createTile(hex);
          if (hex.r == -3) {
            this.createTile(hex);
            this.createStone(hex, this.players[PlayerHeight.Top]);
          }
          if (hex.s == -3) {
            this.createStone(hex, this.players[PlayerHeight.Middle]);
          }
        } else {
          this.createStone(hex, this.players[PlayerHeight.Bottom]);
        }
      }
    });
  }

  protected generateTeleports(): void {
    const teleportSpots = [22, 19, 28, 25, 34, 31];

    this.teleporters = this.corners
      .map(p => {
        // the pointToHex() function considers the center of the hexagon its topleft corner
        return { x: p.x + this.unit / 2, y: p.y + this.unit / 2 };
      })
      .map(p => {
        // turns the edge point coordinates into fresh hexes
        return this.Hex(this.Grid.pointToHex(p));
      })
      .map((hex, i) => {
        // and makes them reference the stack of their teleporting target
        // so essentially, any Phaser function querrying it will think it's the second hex
        hex.referral = this.grid.get(teleportSpots[i]);
        return hex;
      });
  }
}
