import { Player } from "./player";
import { Board } from "./boardLayout";
import { Hex } from "honeycomb-grid";
import { HexTile, Moves, getTopTile } from "./boardSpecs";
import { PlayerHeight } from "../specs";
import shuffle from "../shuffle";

export abstract class AI {
  protected board: Board;
  protected player: Player;

  constructor(board: Board, player?: Player) {
    this.board = board;
    this.player = player;
  }

  reset(board?: Board, player?: Player): void {
    if (board) {
      this.board = board;
    }
    if (player) {
      this.player = player;
    }
  }

  abstract getMove(): SimulatedMove;
}

export class Raindom extends AI {
  getMove(): undefined | SimulatedMove {
    const moves = this.board.setSourceInteractivity(true);

    if (!moves.valid || moves.count == 0) return undefined;

    // always prioritize captures
    if (moves.captures.size > 0) {
      const targets = Array.from(moves.captures);

      return {
        type: Moves.Capture,
        target: targets[Math.floor(Math.random() * targets.length)]
      };
    } else {
      const t = moves.tiles.size;
      const s = moves.stones.size;
      const limit = t / (t + s);

      if (Math.random() < limit) {
        // tile
        const sources = shuffle(Array.from(moves.tiles));
        for (const source of sources) {
          const targets = Array.from(
            this.board.setTileTargetInteractivity(getTopTile(source), true)
          );

          if (targets.length > 0) {
            return {
              type: Moves.Tile,
              source: source,
              target: targets[Math.floor(Math.random() * targets.length)]
            };
          }
        }
      } else {
        // stone
        const sources = Array.from(moves.stones);
        const source = sources[Math.floor(Math.random() * sources.length)];
        const targets = Array.from(
          this.board.setStoneTargetInteractivity(source.stone, true)
        );

        if (targets.length > 0) {
          return {
            type: Moves.Stone,
            source: source,
            target: targets[Math.floor(Math.random() * targets.length)]
          };
        }
      }
    }
  }
}

export class Primai extends AI {
  getMove(): SimulatedMove {
    const moves = this.board.setSourceInteractivity(true);
    if (!moves.valid || moves.count == 0) return undefined;

    // always prioritize captures
    if (moves.captures.size > 0) {
      return {
        type: Moves.Capture,
        target: moves.captures[Math.floor(Math.random() * moves.captures.size)]
      };
    }

    switch (this.board.nrPlayers) {
      case 2:
        break;

      case 3:
        break;
    }

    // moves tile next: clear/create space, but land it wherever (that isn't your own height)
    if (moves.tiles.size > 0) {
      switch (this.player.height) {
        case PlayerHeight.Bottom:
          this.player.stones.forEach;
          break;
        case PlayerHeight.Top:
          break;
        case PlayerHeight.Middle:
          break;
      }

      this.player.edgeDirection;

      // this.board.grid.hexesBetween()
    }

    // moves stones: randomly, but weighted towards the other players
    // if (moves.stones.size > 0) {

    // }

    return undefined;
  }
}

export type SimulatedMove = {
  type: Moves;
  source?: Hex<HexTile>;
  target: Hex<HexTile>;
};
