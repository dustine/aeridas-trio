import PhaserSuperStorage from "@azerion/phaser-super-storage";
import { GameObjects } from "phaser";
import {
  BoardLayout,
  Dialog,
  PlayerHeight,
  PlayerScaffold,
  BoardSceneData,
  playerReg
} from "../specs";
import {
  BoardDuo8,
  BoardTrio12,
  Board,
  BoardDuo10,
  BoardDuo12
} from "./boardLayout";
import { BoardSceneInterface, Moves } from "./boardSpecs";

const width = 864;
const height = 672;
const turnsBetweenConvos = 30;

type Line = {
  line?: string;
  player?: PlayerScaffold;
  guest?: PlayerScaffold;
};

export default class BoardScene extends Phaser.Scene
  implements BoardSceneInterface {
  storage: PhaserSuperStorage;

  board: Board;
  layout: BoardLayout;
  nrPlayers: number;
  bottomPlayer: PlayerScaffold;
  topPlayer: PlayerScaffold;
  middlePlayer: PlayerScaffold;
  ai: boolean[];
  dialogs: Dialog[];
  highlight: boolean;

  roundCounter: GameObjects.Text;
  currentPlayerMarker: GameObjects.Sprite;
  bottomPlayerState: GameObjects.Container;
  topPlayerState: GameObjects.Container;
  middlePlayerState: GameObjects.Container;
  dialogBox: GameObjects.Container;
  dialogProng: GameObjects.Image;

  players: { [x: string]: PlayerScaffold };
  playerHeights: { [x: string]: PlayerHeight };
  rounds: number;
  hasDialogs: boolean;
  isDialogEnabled: boolean;
  turnsUntilConvo: number;
  line: string;
  lineGenerator: Generator<Line, void, unknown>;
  lastFrame: number;
  toggleSound: GameObjects.Image;
  toggleHighlight: GameObjects.Image;

  isHighlighted(): boolean {
    return this.highlight;
  }

  init(data: BoardSceneData): void {
    this.layout = data.layout;
    this.bottomPlayer = data.bottomPlayer;
    this.topPlayer = data.topPlayer;
    this.middlePlayer = data.middlePlayer;
    this.ai = data.cpu;
    this.nrPlayers = this.middlePlayer ? 3 : 2;
    this.dialogs = data.dialogs;
    this.highlight = data.highlight;

    this.players = {
      [this.bottomPlayer.id]: this.bottomPlayer,
      [this.topPlayer.id]: this.topPlayer
    };
    this.playerHeights = {
      [this.bottomPlayer.id]: PlayerHeight.Bottom,
      [this.topPlayer.id]: PlayerHeight.Top
    };
    if (this.middlePlayer) {
      this.players[this.middlePlayer.id] = this.middlePlayer;
      this.playerHeights[this.middlePlayer.id] = PlayerHeight.Middle;
    }
    this.rounds = 0;
    this.hasDialogs = this.dialogs.length > 0;
    this.turnsUntilConvo = turnsBetweenConvos;
    this.line = "";

    this.lineGenerator = function* yieldNextLine(): {} {
      for (const dialog of this.dialogs) {
        const id = `d:${dialog.id}`;
        let ci = parseInt(this.storage.getItem(id));
        if (isNaN(ci) || ci < 0) {
          ci = 0;
        }

        for (const convo of dialog.convos) {
          for (const line of convo) {
            if (playerReg.test(line)) {
              // is this it?
              const index = playerReg.exec(line)[0];
              const player = this.players[dialog.characters[parseInt(index)]];

              yield {
                line: (line as string).slice(index.length),
                player: player
              } as Line;
            } else {
              yield { line: line } as Line;
            }
          }

          // ended convo, save data and send empty line to hide the chatty box
          this.storage.setItem(id, `${++ci}`);

          yield {};
        }
      }
    }.bind(this)();

    // this.cameras.main.setBackgroundColor(0xe3dedb);
    this.input.dragDistanceThreshold = 4;
  }

  preload(): void {
    this.storage = this.plugins.get("SuperStorage") as PhaserSuperStorage;
  }

  createPlayerStatus(
    x: number,
    y: number,
    player: PlayerScaffold
  ): GameObjects.Container {
    const container = this.add.container(x, y);
    const icon = this.add.image(0, 0, `player:${player.id}`);
    if (icon.height > 48) {
      icon.setScale(2 / 3, 2 / 3);
    }
    const move1 = this.add.image(-4 - 16, -30, "board:moveNone");
    const move2 = this.add.image(-4 - 16 * 2, -30 - 24, "board:moveNone");
    return container.add([icon, move1, move2]);
  }

  create(): void {
    this.roundCounter = this.add
      .text(16, 8, "Round 1")
      .setFontFamily("Solway, Arial, non-serif")
      .setFontSize(28)
      .setFontStyle("700")
      .setColor("black");

    const playersHeight = height - 68;

    this.currentPlayerMarker = this.add
      .sprite(0, playersHeight - 5, "board:playerMarker")
      .setOrigin(0.73, 0.75);

    this.bottomPlayerState = this.createPlayerStatus(
      10 + 56,
      playersHeight,
      this.bottomPlayer
    );
    this.topPlayerState = this.createPlayerStatus(
      10 + 56 * 2,
      playersHeight,
      this.topPlayer
    );
    if (this.nrPlayers == 3) {
      this.middlePlayerState = this.createPlayerStatus(
        10 + 56 * 3,
        playersHeight,
        this.middlePlayer
      );
    }
    this.currentPlayerMarker.setX(this.bottomPlayerState.x);

    const dialogHeight = this.currentPlayerMarker.getBottomCenter().y + 5;

    this.dialogBox = this.add.container(32, dialogHeight + 18, [
      this.add.image(0, 0, "board:dialog").setOrigin(0, 0.5),
      this.add
        .text(12, -9, "")
        .setFontFamily("Solway")
        .setFontStyle("700")
        .setFontSize(16)
        .setColor("black"),
      this.add
        .text(12, -8, "")
        .setFontFamily("Solway")
        .setFontStyle("300")
        .setFontSize(16)
        .setColor("black")
    ]);

    this.dialogBox.first.setInteractive().on(
      "pointerup",
      () => {
        const lineText = this.dialogBox.last as GameObjects.Text;
        if (this.line.length != 0) {
          lineText.text += this.line;
          this.line = "";
        } else {
          this.updateLine();
        }
      },
      this
    );

    this.dialogProng = this.add
      .image(this.bottomPlayerState.x, dialogHeight - 2, "board:dialogProng")
      .setVisible(false);

    const exit = this.add.image(width - 32, 32, "board:exit").setInteractive();
    exit.on("pointerup", () => {
      this.scene.start("MenuScene");
    });
    exit.input.cursor = "pointer";

    const bottomBar = { x: 0, y: 0 };
    bottomBar.x =
      (this.dialogBox.first as GameObjects.Image).getRightCenter().x +
      this.dialogBox.x;
    bottomBar.y =
      (this.dialogBox.first as GameObjects.Image).getRightCenter().y +
      this.dialogBox.y;

    const muteSetting = parseInt(this.storage.getItem("m"));
    this.toggleSound = this.add
      .image(
        bottomBar.x + 32,
        bottomBar.y,
        muteSetting ? "board:mute" : "board:sound"
      )
      .setInteractive()
      .setState(muteSetting);

    const highlightSetting = parseInt(this.storage.getItem("h"));
    this.toggleHighlight = this.add
      .image(
        bottomBar.x + 16 + 32 * 2,
        bottomBar.y,
        highlightSetting ? "board:plain" : "board:highlight"
      )
      .setInteractive()
      .setState(highlightSetting);
    this.highlight = !highlightSetting;

    this.toggleSound.on("pointerdown", this.onToggleSound, this);

    this.toggleHighlight.on("pointerdown", this.onToggleHighlight, this);

    const boardData = {
      scene: this,
      x: width / 2,
      y: height / 2,
      depth: 0,
      players: [this.bottomPlayer, this.topPlayer, this.middlePlayer],
      ai: this.ai
    };

    switch (this.layout) {
      default:
      case BoardLayout.Duo8:
        this.board = new BoardDuo8(boardData);
        break;
      case BoardLayout.Duo10:
        this.board = new BoardDuo10(boardData);
        break;
      case BoardLayout.Duo12:
        this.board = new BoardDuo12(boardData);
        break;
      case BoardLayout.Trio12:
        this.board = new BoardTrio12(boardData);
        break;
    }

    this.updateLine();
  }

  private onToggleHighlight(): void {
    switch (this.toggleHighlight.state) {
      case 1:
        this.toggleHighlight.setTexture("board:highlight").setState(0);
        this.storage.setItem("h", "" + 0);
        this.highlight = true;
        break;
      case 0:
      default:
        this.toggleHighlight.setTexture("board:plain").setState(1);
        this.storage.setItem("h", "" + 1);
        this.highlight = false;
        break;
    }

    this.board.toggleHighlight();
  }

  private onToggleSound(): void {
    switch (this.toggleSound.state) {
      case 1:
        this.toggleSound.setTexture("board:sound").setState(0);
        this.storage.setItem("m", "" + 0);
        break;
      case 0:
      default:
        this.toggleSound.setTexture("board:mute").setState(1);
        this.storage.setItem("m", "" + 1);
        break;
    }
  }

  update(time: number, _delta: number): void {
    if (!this.lastFrame) this.lastFrame = time;
    if (this.line.length != 0 && time - this.lastFrame > 4) {
      const lineText = this.dialogBox.last as GameObjects.Text;
      const character = this.line[0];
      this.line = this.line.slice(1);
      lineText.text = lineText.text + character;
      this.lastFrame = time;
    }
  }

  getPlayerStats(player: PlayerHeight): GameObjects.Container {
    let playerStats: GameObjects.Container;
    switch (player) {
      case PlayerHeight.Bottom:
        playerStats = this.bottomPlayerState;
        break;
      case PlayerHeight.Top:
        playerStats = this.topPlayerState;
        break;
      case PlayerHeight.Middle:
        playerStats = this.middlePlayerState;
        break;
    }
    return playerStats;
  }

  updateMoves(moves: Moves[], player: PlayerHeight): void {
    const playerStats = this.getPlayerStats(player);

    moves.forEach((move, i) => {
      let texture: string;
      switch (move) {
        case Moves.Stone:
          texture = "board:moveStone";
          break;
        case Moves.Tile:
          texture = "board:moveTile";
          break;
        case Moves.Capture:
          texture = "board:moveCapture";
          break;
        default:
        case Moves.None:
          texture = "board:moveNone";
          break;
      }
      (playerStats.list[i + 1] as GameObjects.Image).setTexture(texture);
    });
  }

  newTurn(player: PlayerHeight): void {
    if (this.turnsUntilConvo-- <= 0) {
      if (this.hasDialogs && !this.isDialogEnabled) {
        this.turnsUntilConvo = turnsBetweenConvos;
        this.updateLine();
      }
    }

    this.updateMoves([Moves.None, Moves.None], player);
    const playerStats = this.getPlayerStats(player);
    this.tweens.add({
      targets: this.currentPlayerMarker,
      ease: "Bounce",
      duration: 500,
      x: { from: this.currentPlayerMarker.x, to: playerStats.x }
    });
  }

  updateDialogBox(line: Line): void {
    const dialogBackground = this.dialogBox.first as GameObjects.Image;
    const nameText = this.dialogBox.list[1] as GameObjects.Text;
    const lineText = this.dialogBox.last as GameObjects.Text;
    if (line.line) {
      this.isDialogEnabled = true;
      dialogBackground.clearTint().setInteractive();

      if (line.player) {
        const playerStats = this.getPlayerStats(
          this.playerHeights[line.player.id]
        );
        nameText.setText(`${line.player.name}: `);
        lineText.setFontStyle("300");
        this.dialogProng.setVisible(true).setX(playerStats.x);
      } else {
        nameText.setText("");
        lineText.setFontStyle("300 italic");
        this.dialogProng.setVisible(false);
      }
      lineText.setX(12 + nameText.width);
      lineText.setText("");

      this.line = line.line;
    } else {
      this.isDialogEnabled = false;
      this.dialogProng.setVisible(false);
      dialogBackground.setTint(0x333333).disableInteractive();
      nameText.setText("");
      lineText.setText("");

      this.line = "";
    }
  }

  updateLine(): void {
    if (this.line.length == 0) {
      const nextLine = this.lineGenerator.next();
      if (nextLine.done) {
        const dialogBackground = this.dialogBox.first as GameObjects.Image;
        dialogBackground.setTint(0x333333).disableInteractive();
        this.hasDialogs = false;
      } else {
        if (nextLine.value) {
          this.updateDialogBox(nextLine.value);
        }
      }
    }
  }

  newRound(): void {
    this.roundCounter.setText(`Round ${++this.rounds + 1}`);
  }

  lost(player: PlayerHeight): void {
    const playerStats = this.getPlayerStats(player);

    (playerStats.first as GameObjects.Image).setTint(0x333333);
  }
}
