/* eslint-disable @typescript-eslint/no-empty-function  */
/* eslint  @typescript-eslint/no-use-before-define: ["error", { "classes": false }] */
import { Hex } from "honeycomb-grid";
import {
  HexTile,
  getTileHeight,
  getTopTile,
  TileState,
  Moves
} from "./boardSpecs";
import { Player } from "./player";
import { GameObjects, Input } from "phaser";
import { Board } from "./boardLayout";
import { SimulatedMove } from "./ai";

type Stone = GameObjects.Container;
type Tile = GameObjects.Sprite;

const selectTint = 0x696969;
const hoverTint = 0xffffff - selectTint;
// const hoverTint = 0xcccccc

export abstract class BoardState {
  protected board: Board;

  public setBoard(board: Board): void {
    this.board = board;
  }

  onTilePointerDown(tile: Tile): void {
    tile.setTint(selectTint);
  }
  onTilePointerOver(tile: Tile): void {
    tile.setTint(hoverTint);
  }
  onTilePointerOut(tile: Tile): void {
    tile.clearTint();
  }
  onTilePointerUp(tile: Tile): void {
    tile.clearTint();
  }

  abstract onTileDragStart(tile: Tile): void;
  abstract onTileDragEnter(tile: Tile, target: GameObjects.GameObject): void;
  abstract onTileDrag(
    tile: Tile,
    pointer: Phaser.Input.Pointer,
    x: number,
    y: number
  ): void;
  abstract onTileDragLeave(tile: Tile, target: GameObjects.GameObject): void;
  abstract onTileDragEnd(tile: Tile): void;

  onStonePointerDown(stone: Stone): void {
    const tile = getTopTile(stone.data.values.hex);
    tile.setTint(selectTint);
  }
  onStonePointerOver(stone: Stone): void {
    const tile = getTopTile(stone.data.values.hex);
    tile.setTint(hoverTint);
  }
  onStonePointerOut(stone: Stone): void {
    const tile = getTopTile(stone.data.values.hex);
    tile.clearTint();
  }
  onStonePointerUp(stone: Stone): void {
    const tile = getTopTile(stone.data.values.hex);
    tile.clearTint();
  }

  abstract onStoneDragStart(stone: Stone): void;
  abstract onStoneDragEnter(stone: Stone, target: GameObjects.GameObject): void;
  abstract onStoneDrag(
    stone: Stone,
    pointer: Phaser.Input.Pointer,
    x: number,
    y: number
  ): void;
  abstract onStoneDragLeave(stone: Stone, target: GameObjects.GameObject): void;
  abstract onStoneDragEnd(stone: Stone): void;
}

/* eslint-disable @typescript-eslint/no-empty-function  */

abstract class TileOnlyState extends BoardState {
  onStoneDragStart(_stone: Stone): void {}
  onStoneDragEnter(_stone: Stone, _target: GameObjects.GameObject): void {}
  onStoneDrag(
    _stone: Stone,
    _pointer: Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onStoneDragLeave(_stone: Stone, _target: GameObjects.GameObject): void {}
  onStoneDragEnd(_stone: Stone): void {}
}

abstract class StoneOnlyState extends BoardState {
  onTileDragStart(_tile: Tile): void {}
  onTileDragEnter(_tile: Tile, _target: GameObjects.GameObject): void {}
  onTileDrag(
    _tile: Tile,
    _pointer: Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onTileDragLeave(_tile: Tile, _target: GameObjects.GameObject): void {}
  onTileDragEnd(_tile: Tile): void {}
}

abstract class ProcessingOnlyState extends BoardState {
  onTilePointerDown(_tile: Tile): void {}
  onTilePointerUp(_tile: Tile): void {}
  onTileDragStart(_tile: Tile): void {}
  onTileDragEnter(_tile: Tile, _target: GameObjects.GameObject): void {}
  onTileDrag(
    _tile: Tile,
    _pointer: Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onTileDragLeave(_tile: Tile, _target: GameObjects.GameObject): void {}
  onTileDragEnd(_tile: Tile): void {}
  onTilePointerOver(_tile: Tile): void {}
  onTilePointerOut(_tile: Tile): void {}
  onStonePointerDown(_stone: Stone): void {}
  onStonePointerUp(_stone: Stone): void {}
  onStoneDragStart(_stone: Stone): void {}
  onStoneDragEnter(_stone: Stone, _target: GameObjects.GameObject): void {}
  onStoneDrag(
    _stone: Stone,
    _pointer: Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onStoneDragLeave(_stone: Stone, _target: GameObjects.GameObject): void {}
  onStoneDragEnd(_stone: Stone): void {}
  onStonePointerOver(_stone: Stone): void {}
  onStonePointerOut(_stone: Stone): void {}
}
export class NeutralState extends BoardState {
  setBoard(board: Board): void {
    super.setBoard(board);

    if (this.board.currentMoves.length >= 2) {
      this.board.setNextPlayer();
    }

    if (this.board.currentPlayer.ai) {
      this.board.disableBoard();
      this.board.transitionTo(new AiState());
      return;
    } else {
      const targets = this.board.setSourceInteractivity();

      // LOSING CONDITION
      //  ran out of moves
      if (targets.count == 0) {
        this.board.lose(this.board.currentPlayer);
        this.board.transitionTo(new PlayerLostState(this.board.currentPlayer));
        return;
      }
    }
  }

  onTilePointerUp(tile: Tile): void {
    super.onTilePointerUp(tile);
    // no dragging (for now) so just on Up is enough
    this.board.setTileTargetInteractivity(tile);

    this.board.transitionTo(new SelectedTileState(tile));
  }

  onTileDragStart(tile: Tile): void {
    // bottom tiles can't be dragged
    if (getTileHeight(tile.state as TileState) == TileState.Bottom) return;

    this.board.setTileTargetInteractivity(tile);

    // disable this tile so it doesn't consider itself a dropzone, lol
    tile
      .clearTint()
      .setDepth(50)
      .setAlpha(0.75)
      .setTexture("board:tileFull")
      .disableInteractive();
    const hex = tile.data.values.hex as Hex<HexTile>;
    // unrisens the previous tile, but keeps it without interactivity
    const previousTile = hex.stack[hex.stack.length - 2];
    this.board.setTileState(
      previousTile,
      (previousTile.state as TileState) & ~TileState.Rise
    );

    // finally, we start the drag
    this.board.transitionTo(new DraggingTileState());
  }
  onTileDragEnter(): void {}
  onTileDrag(): void {}
  onTileDragLeave(): void {}
  onTileDragEnd(): void {}

  onStonePointerDown(_stone: Stone): void {}

  onStonePointerUp(stone: Stone): void {
    super.onStonePointerUp(stone);

    if (stone.data.values.player == this.board.currentPlayer) {
      // stone move
      // sets only the neigbour tiles (and the stone itself) as interactable
      this.board.setStoneTargetInteractivity(stone);

      this.board.transitionTo(new SelectedStoneState(stone));
    } else {
      // finish the capture
      this.board.destroyStone(stone);

      // action done, back to neutral
      this.board.transitionTo(new EndedMoveState(Moves.Capture));
    }
  }

  onStoneDragStart(stone: Stone): void {
    const tile = getTopTile(stone.data.values.hex);
    tile.clearTint();

    // stone move
    // sets only the neigbour tiles (and the stone itself) as interactable
    this.board.setStoneTargetInteractivity(stone);

    stone.setDepth(50);
    this.board.transitionTo(new DraggingStoneState());
  }

  onStoneDragEnter(): void {}
  onStoneDrag(): void {}
  onStoneDragLeave(): void {}
  onStoneDragEnd(): void {}
}

class AiState extends ProcessingOnlyState {
  move: SimulatedMove;
  timer: Phaser.Time.TimerEvent;

  setBoard(board: Board): void {
    super.setBoard(board);

    const aiPlayer = this.board.currentPlayer;
    const move = aiPlayer.ai.getMove();

    // invalid moves make the AI lose immediately
    if (!move || move.type == Moves.None) {
      this.board.lose(aiPlayer);
      this.board.transitionTo(new PlayerLostState(aiPlayer));
      return;
    }

    this.move = move;
    this.timer = this.board.scene.time.delayedCall(
      1500,
      this.afterThinking,
      undefined,
      this
    );
    // workaround so pointerup's from other states don't interfere with this one
    this.board.scene.time.delayedCall(
      50,
      function() {
        this.board.scene.input.on("pointerup", this.onPointerUp, this);
      },
      undefined,
      this
    );
  }

  afterThinking(): void {
    if (this.move.source) {
      this.board.setTileState(getTopTile(this.move.source), TileState.Selected);

      this.timer = this.board.scene.time.delayedCall(
        500,
        this.afterSelecting,
        undefined,
        this
      );
    } else {
      this.afterSelecting();
    }
  }

  onPointerUp(): void {
    // hurry up!!!!
    this.timer.remove();
    this.afterSelecting();
  }

  afterSelecting(): void {
    // just in case
    this.timer.remove();
    this.board.scene.input.off("pointerup", this.onPointerUp, this);

    switch (this.move.type) {
      case Moves.Tile:
        this.board.moveTile(this.move.source, this.move.target);
        break;
      case Moves.Stone:
        this.board.moveStone(this.move.source, this.move.target);
        break;
      case Moves.Capture:
        this.board.destroyStone(this.move.target.stone);
        break;
    }

    this.board.transitionTo(new EndedMoveState(this.move.type));
  }
}

class DraggingTileState extends TileOnlyState {
  target: GameObjects.Sprite;

  onTilePointerUp(tile: GameObjects.Sprite): void {
    super.onTilePointerUp(tile);

    if (this.target) {
      this.board.transitionTo(new EndedMoveState(Moves.Tile));
    }
  }

  onTileDragStart(_tile: GameObjects.Sprite): void {}
  onTileDragEnter(
    tile: GameObjects.Sprite,
    target: GameObjects.GameObject
  ): void {
    // just to be 100% sure self-selection doesn't happen
    if (tile != target) {
      this.target = (target as GameObjects.Sprite).setTint(hoverTint);
    }
  }
  onTileDrag(
    tile: GameObjects.Sprite,
    _pointer: Phaser.Input.Pointer,
    x: number,
    y: number
  ): void {
    tile.setX(x).setY(y);
  }
  onTileDragLeave(
    _tile: GameObjects.Sprite,
    _target: GameObjects.GameObject
  ): void {
    // because of self-assignment, this event can fire without a target, hence the check
    if (this.target) {
      this.target.clearTint();
      this.target = undefined;
    }
  }
  onTileDragEnd(tile: GameObjects.Sprite): void {
    // gotta reset the dragging adjustments ^^"
    tile.setAlpha(1).setInteractive();

    const sourceHex = tile.data.values.hex;
    if (this.target) {
      this.board.moveTile(sourceHex, this.target.data.values.hex);
    } else {
      // this fixes both position, depth and texture all in one go so it's perfect
      this.board.moveTile(sourceHex, sourceHex);
      this.board.transitionTo(new SelectedTileState(tile));
    }
  }
}

class SelectedTileState extends TileOnlyState {
  private tile: Tile;

  constructor(tile: Tile) {
    super();
    this.tile = tile;
  }

  onTilePointerUp(tile: Tile): void {
    super.onTilePointerUp(tile);

    if (tile == this.tile) {
      // cancel
      this.board.transitionTo(new NeutralState());
    } else {
      // do the swappy swappy
      this.board.moveTile(this.tile.data.values.hex, tile.data.values.hex);
      // action done, back to neutral
      this.board.transitionTo(new EndedMoveState(Moves.Tile));
    }
  }

  onTileDragStart(tile: Tile): void {
    // bottom tiles can't be dragged
    if (getTileHeight(tile.state as TileState) == TileState.Bottom) return;

    // disable this tile so it doesn't consider itself a dropzone, lol
    tile
      .clearTint()
      .setDepth(50)
      .setAlpha(0.75)
      .setTexture("board:tileFull")
      .disableInteractive();

    const hex = tile.data.values.hex as Hex<HexTile>;
    // unrisens the previous tile, but keeps it without interactivity
    const previousTile = hex.stack[hex.stack.length - 2];
    this.board.setTileState(
      previousTile,
      (previousTile.state as TileState) & ~TileState.Rise
    );

    // finally, we start the drag
    this.board.transitionTo(new DraggingTileState());
  }
  onTileDragEnter(): void {}
  onTileDrag(): void {}
  onTileDragLeave(): void {}
  onTileDragEnd(): void {}
}

class DraggingStoneState extends StoneOnlyState {
  target: GameObjects.Sprite;

  // to prevent a drag bug, we move the neutral state change here
  onStonePointerUp(stone: Stone): void {
    super.onStonePointerUp(stone);

    // action done, back to neutral
    if (this.target) {
      this.board.transitionTo(new EndedMoveState(Moves.Stone));
    }
  }

  onStoneDragStart(): void {}
  onStoneDragEnter(_stone: Stone, target: GameObjects.GameObject): void {
    if (target.name == "tile") {
      (target as Tile).setTint(hoverTint);
      this.target = target as GameObjects.Sprite;
      // stone.data.set("target", target);
    }
  }
  onStoneDrag(
    stone: Stone,
    _pointer: Phaser.Input.Pointer,
    x: number,
    y: number
  ): void {
    stone.x = x;
    stone.y = y;
  }
  onStoneDragLeave(_stone: Stone, target: GameObjects.GameObject): void {
    if (target.name == "tile") {
      (target as Tile).clearTint();
      this.target = undefined;
      // stone.data.remove("target");
    }
  }
  onStoneDragEnd(stone: Stone): void {
    if (this.target) {
      // clear tint off the old tile
      this.target.clearTint();
      // move to the new tile
      this.board.moveStone(stone.data.values.hex, this.target.data.values.hex);
    } else {
      // bounce back to the previous tile, BUT
      // "continue" selected, after all
      this.board.resetStone(stone.data.values.hex);
      this.board.transitionTo(new SelectedStoneState(stone));
    }
  }
}

class SelectedStoneState extends StoneOnlyState {
  private stone: Stone;

  constructor(stone: Stone) {
    super();
    this.stone = stone;
  }

  onTilePointerUp(tile: Tile): void {
    super.onTilePointerUp(tile);

    // move to the new tile
    this.board.moveStone(this.stone.data.values.hex, tile.data.values.hex);
    // action done, back to neutral
    this.board.transitionTo(new EndedMoveState(Moves.Stone));
  }

  onStonePointerUp(stone: Stone): void {
    super.onStonePointerUp(stone);
    this.board.transitionTo(new NeutralState());
  }

  onStoneDragStart(stone: Stone): void {
    const tile = getTopTile(stone.data.values.hex);
    tile.clearTint();

    stone.setDepth(50);
    this.board.transitionTo(new DraggingStoneState());
  }
  onStoneDragEnter(_stone: Stone, _target: GameObjects.GameObject): void {}
  onStoneDrag(
    _stone: Stone,
    _pointer: Phaser.Input.Pointer,
    _x: number,
    _y: number
  ): void {}
  onStoneDragLeave(_stone: Stone, _target: GameObjects.GameObject): void {}
  onStoneDragEnd(_stone: Stone): void {}
}

class EndedMoveState extends ProcessingOnlyState {
  move: Moves;

  constructor(move: Moves) {
    super();
    this.move = move;
  }

  setBoard(board: Board): void {
    super.setBoard(board);

    // action done, back to neutral
    this.board.currentMoves.push(this.move);
    this.board.scene.updateMoves(
      this.board.currentMoves,
      this.board.currentPlayer.height
    );

    // WINNING CONDITIONS
    const otherPlayers = this.board.players.filter(player => {
      return !player.lost && player != this.board.currentPlayer;
    });

    for (let pi = 0; pi < otherPlayers.length; pi++) {
      const player = otherPlayers[pi];
      // captured all of the opponent's stones
      if (player.stones.size == 0) {
        this.board.lose(player);
        this.board.transitionTo(new PlayerLostState(player));
        return;
      }

      const stones = Array.from(this.board.currentPlayer.stones);
      for (let si = 0; si < stones.length; si++) {
        const stone = stones[si];
        // reached the opponent's side
        if (this.board.isEdge(stone.data.values.hex, player)) {
          this.board.lose(player);
          this.board.transitionTo(new PlayerLostState(player));
          return;
        }
      }
    }

    this.board.transitionTo(new NeutralState());
  }
}

class PlayerLostState extends ProcessingOnlyState {
  loser: Player;
  constructor(loser: Player) {
    super();
    this.loser = loser;
  }

  setBoard(board: Board): void {
    super.setBoard(board);

    if (this.board.players.length > 2) {
      // uh oh, game's not over

      if (this.board.currentPlayer == this.loser) {
        this.board.setNextPlayer();
      }

      const index = this.board.players.indexOf(this.loser);
      this.board.players.splice(index, 1);

      // and the game moves on as normal
      this.board.transitionTo(new NeutralState());
      return;
    }

    // for good measure, *make sure to disable the board*
    this.board.disableBoard();
  }
}
