import { GameObjects } from "phaser";
import { FlatCompassDirection, PlayerHeight, PlayerScaffold } from "../specs";
import { Raindom, AI } from "./ai";
import { Board } from "./boardLayout";

export class Player {
  name: string;
  base: number;
  ribbon: number;
  height: PlayerHeight;
  edgeDirection: FlatCompassDirection;
  edge: GameObjects.Container;
  ai: AI;
  stones: Set<GameObjects.Container>;
  lost: boolean;

  constructor(spec: PlayerScaffold) {
    this.name = spec.name;
    this.base = spec.base;
    this.ribbon = spec.ribbon;
    this.stones = new Set<GameObjects.Container>();
    this.lost = false;
  }

  artificialize(board: Board, ai?: AI): void {
    if (ai) {
      this.ai = ai;
      ai.reset(board, this);
    } else {
      this.ai = new Raindom(board, this);
    }
  }
}
