import { Hex } from "honeycomb-grid";
import { GameObjects } from "phaser";
import { PlayerHeight, PlayerScaffold } from "../specs";

export enum TileState {
  None = 0,
  Bottom = 1,
  Middle = 2,
  Top = 3,
  Rise = 1 << 2,
  Selected = 1 << 3,
  Source = 1 << 4,
  Target = 1 << 5,
  Capture = 1 << 6
}

export function getTileHeight(state: TileState): TileState {
  return state & 0b11;
}

export type HexTile = {
  size: { width: number; height: number };
  stack: GameObjects.Sprite[];
  stone?: GameObjects.Container;
  referral?: Hex<HexTile>;
};

export function getTopTile(hex: Hex<HexTile>): GameObjects.Sprite {
  return hex.stack[hex.stack.length - 1];
}

export type BoardData = {
  scene: Phaser.Scene & BoardSceneInterface;
  x: number;
  y: number;
  depth: number;
  players: PlayerScaffold[];
  ai: boolean[];
};

export interface BoardSceneInterface {
  isHighlighted(): boolean;
  updateMoves(moves: Moves[], player: PlayerHeight): void;
  newTurn(player: PlayerHeight): void;
  newRound(): void;
  lost(player: PlayerHeight): void;
}

export enum Moves {
  None = 0,
  Tile = 1 << 0,
  Stone = 1 << 1,
  Capture = 1 << 2
}
