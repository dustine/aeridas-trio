import WebFont from "webfontloader";
import "phaser";

import PhaserSuperStorage from "@azerion/phaser-super-storage";
import BootScene from "./boot";
import { Dialog, PlayerScaffold, playerReg } from "./specs";

const config = {
  type: Phaser.AUTO,
  // backgroundColor: "#bbaa88",
  backgroundColor: "#d1c5ad",
  width: 864,
  height: 672,
  name: "Trio",
  parent: "game",
  scene: BootScene,
  plugins: {
    global: [{ key: "SuperStorage", plugin: PhaserSuperStorage, start: true }]
  },
  render: {
    roundPixels: true
  }
} as Phaser.Types.Core.GameConfig;

let dialogs: { [x: string]: Dialog };
fetch("./assets/dialog.json")
  .then(response => {
    return response.json();
  })
  .then(body => {
    dialogs = body;
  });

const players: { [x: string]: PlayerScaffold } = {};
fetch("./assets/players.json")
  .then(response => {
    return response.json();
  })
  .then((body: PlayerScaffold[]) => {
    for (const player of body) {
      players[player.id] = player;
    }
  });

function parseConvo(
  index: number,
  convo: string[],
  characters: string[]
): Node {
  const $node = document.createElement("div");
  $node.classList.add("convo");
  for (const line of convo) {
    const $line = document.createElement("p");
    $line.classList.add("line");
    if (playerReg.test(line)) {
      const pi = playerReg.exec(line)[0];
      const player = players[characters[parseInt(pi)]];

      $line.classList.add("player-line");
      const $player = document.createElement("span");
      $player.classList.add("player");
      $player.append(player.name);
      $line.append($player);
      $line.append(line.slice(pi.length));
      $node.append($line);
      // dialog
    } else {
      $line.classList.add("action-line");
      $line.append(line);
      $node.append($line);
      // action
    }
  }

  return $node;
}

function parseDialog(dialog: Dialog, range: number): Node {
  const $node = document.createElement("div");
  const $title = document.createElement("h2");

  const title = dialog.characters
    .map(s => {
      return players[s].name;
    })
    .join(", ");
  $title.append(title + " ");
  for (const player of dialog.characters) {
    const $img = document.createElement("img");
    $img.setAttribute("src", `assets/players/${player}.png`);
    $img.setAttribute("alt", player);
    $img.classList.add("player");
    $title.append($img);
  }
  $node.append($title);
  for (let index = 0; index < range || index < dialog.convos.length; index++) {
    const convo = dialog.convos[index];
    $node.append(parseConvo(index, convo, dialog.characters));
  }

  return $node;
}

function startGame(): void {
  const $transcripts = document.getElementById("transcript");
  $transcripts.style.setProperty("visibility", "visible");

  const $title = $transcripts.firstElementChild;
  const $dialogs = document.getElementById("dialogs");
  const $reset = document.getElementById("reset");

  $title.addEventListener("click", function() {
    const $dialogs = document.getElementById("dialogs");
    if ($dialogs.toggleAttribute("active")) {
      $title.innerHTML = "Transcript ⏶";

      for (const id in dialogs) {
        const dialog = dialogs[id] as Dialog;
        const history = parseInt(localStorage.getItem(`aeridas-trio:d:${id}`));
        if (history && history > 0) {
          $dialogs.append(parseDialog(dialog, history));
        }
      }

      if ($dialogs.firstChild) {
        // add ALL the children.
        // show reset
        $reset.style.setProperty("visibility", "visible");
      } else {
        // show a placeholder if there's no dialogs
        const placeholder = document.createElement("i");
        placeholder.innerText = "None, yet.";
        $dialogs.appendChild(placeholder);
      }
    } else {
      // reset, clear all children
      $title.innerHTML = "Transcript ⏷";
      while ($dialogs.firstChild) {
        $dialogs.removeChild($dialogs.firstChild);
      }
      $reset.style.setProperty("visibility", "hidden");
    }
  });

  $reset.addEventListener("click", function() {
    for (let i = 0; i < localStorage.length; i++) {
      for (let index = localStorage.length - 1; index >= 0; index--) {
        const key = localStorage.key(index);
        if (key.startsWith("aeridas-trio:d:")) {
          localStorage.removeItem(key);
        }
      }
      // collapses the transcripts
      $title.innerHTML = "Transcript ⏷";
      while ($dialogs.firstChild) {
        $dialogs.removeChild($dialogs.firstChild);
      }
      $reset.style.setProperty("visibility", "hidden");
    }
  });

  const $game = document.getElementById("game");
  while ($game.firstChild) {
    $game.removeChild($game.firstChild);
  }
  new Phaser.Game(config);
}

WebFont.load({
  google: {
    families: ["Solway:300,700"]
  },
  active: startGame,
  // TODO: workaround if we don't have fonts, but try first!
  inactive: startGame
});
